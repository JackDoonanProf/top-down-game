import java.awt.Point;

public class CharacterAwareness {

	public int id;
	public String nameOfCharacter = "unknown";
	public String equippedItem = "none";
	public String stance = "normal";
	public String gender = "unknown";
	public int attractionTo = 0;
	public int intimidatedBy = 5;
	public String factionOfCharacter = "unknown";
	public Point locationOfCharacter = new Point(0,0);
	
	public CharacterAwareness(int i){
		this.id = i;
	}
}
