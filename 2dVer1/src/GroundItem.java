import java.awt.Point;
import java.awt.Polygon;
import java.util.ArrayList;

public class GroundItem extends Entity {

	Item i;
	//0 = away, 1 = hover, 2 = clickDown, 3 = released
	
	public GroundItem(int x, int y, String n,String filename) {
		super(x, y, n,filename);
		// TODO Auto-generated constructor stub
		int w = this.texture.getWidth();
		int h = this.texture.getHeight();
		
		Point i1 = new Point(0, 0);
		Point i2 = new Point(w, 0);
		Point i3 = new Point(w, h);
		Point i4 = new Point(0, h);
		Point[] boint = { i1, i2, i3, i4 };
		this.vertexes = boint;
		this.originalVertexes = boint;
		
		this.updateTexture("dropItem.png");
		this.clickState = 0;
	}
	
	
	public void update(KeybMouse keybstar){
		this.clickState = calculateLeftClickState(new Point(keybstar.xCoord, keybstar.yCoord),keybstar.lmb);
		
		if(clickState == 0 || clickState == 3){
			this.updateTexture("dropItem.png");
		}
		if(clickState == 1){
			this.updateTexture("dropItemHover.png");
		}
		if(clickState == 2){
			this.updateTexture("dropItemClick.png");
		}		
		xPos += xVel;
		yPos += yVel;
	}
	
	public int calculateLeftClickState(Point mouse,boolean click){
		if(click){
			System.out.println("CLICK");
		}
		int lastState = this.clickState;
		int c = 0;
		
		Polygon thispoly = new Polygon();
		int h = this.texture.getHeight();
		System.out.println("YPOS" + (int)this.yPos);
		System.out.println("MOUSEY" + mouse.y);
		for(int i = 0; i < vertexes.length;i++){
			thispoly.addPoint(this.vertexes[i].x + (int)this.xPos,this.vertexes[i].y +(int)this.yPos);
		}
		System.out.println(thispoly.xpoints[0]);
		System.out.println(mouse.x);
		if(thispoly.contains(mouse)){
			c = 1;
			if(click){
				c = 2;
			}
		 }
		
		
		//releasing after clicking on object
		if(lastState == 2 && c == 1)return 3;
		//Hovering
		if(c == 1)return c;
		//Clicking Down
		if(c == 2)return c;
		//releasing away from the object
		return 0;
	}
	
	public ArrayList<String> getClickOptions(){
		ArrayList<String> al = new ArrayList<String>();
		al.add("inspect");
		al.add("pickup");
		return al;
	}
	
	public void setItem(Item _i){
		this.i = _i;
	}
	
	public Item getItem(){
		return this.i;
	}
	
}
