import java.awt.Color;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;

public class Arrow extends Projectile {
	int damage = 1;
	static BufferedImage funt;
	
	public Arrow(int x, int y, String n, Point[] v, Color c, double angle, double velocity) {
		super(x, y, n, v, c, funt, angle, velocity);
		try {
			funt = ImageIO.read(new File("arrow.png"));
			
		} catch (IOException e) {
		}
		this.texture = funt;
		// TODO Auto-generated constructor stub
	}
	@Override
	public String update(ArrayList<Entity> entList,ArrayList<Character> charList,ArrayList<Door> doorList){
		String command = "";
		this.yPos -= Math.cos(Math.toRadians(direction))*velocity;
		this.xPos += Math.sin(Math.toRadians(direction))*velocity;
		
		for(Character c : charList){
			Point bp = new Point((int)xPos,(int)yPos);
			Polygon charpoly = new Polygon();
			for(int i = 0; i < c.vertexes.length;i++){
				charpoly.addPoint((int)(c.vertexes[i].x+c.xPos),(int)(c.vertexes[i].y+c.yPos));
			}
			if(charpoly.contains(bp)){
				c.Damage(damage);
				this.hit = true;
				break;
			}
		}
		velocity -=5;
		if(velocity <= 0){
			this.hit = true;
		}
		return command;
	}
}
