import java.util.HashMap;

public class Graph {

	HashMap<String,Node> nodes;
	
	public Graph(){
		this.nodes = new HashMap<String,Node>();
	}
	
	public void addNode(int x, int y){
		Node n = new Node(x,y);
		this.nodes.put(n.name,n);
	}
	
	public void addEdge(String n1, String n2){
		this.nodes.get(n1).addConnection(this.nodes.get(n2));
	}
}
