import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

public class DialogueText extends Entity {
	String text;
	int screenWidth;
	int screenHeight;
	int screenOrder;
	ArrayList<String> triggerNames;

	public DialogueText(int x, int y, String n) {
		super(x, y, n);
		this.screenOrder = 1;
		this.vertexes = new Point[4];
		vertexes[0] = new Point(0,0);
		vertexes[1] = new Point(0,0);
		vertexes[2] = new Point(0,0);
		vertexes[3] = new Point(0,0);
		this.triggerNames = new ArrayList<String>();
		// TODO Auto-generated constructor stub
	}
	public void setText(String t){
		this.text = t;
	}
	public void setEvents(ArrayList<String> e){
		this.triggerNames = e;
	}
	public ArrayList<String> getEvents(){
		return triggerNames;
	}

	public Graphics draw(Graphics g, GameLoopTest.GamePanel gp, Camera c){
		//rotation ++;
		this.screenWidth = c.SCREEN_WIDTH;
		this.screenHeight = c.SCREEN_HEIGHT;
		Polygon poly = new Polygon();

		int xLeft = (int)(.2*screenWidth);
		int xRight = (int)(.8*screenWidth);
		int yTop = (int) (.42*screenHeight + (45 * screenOrder));
		int yBot = yTop + 45;
		
		poly.addPoint(xLeft,yTop);
		vertexes[0] = new Point(xLeft,yTop);
		poly.addPoint(xRight,yTop);
		vertexes[1] = new Point(xRight,yTop);
		poly.addPoint(xRight,yBot);
		vertexes[2] = new Point(xRight,yBot);
		poly.addPoint(xLeft,yBot);
		vertexes[3] = new Point(xLeft,yBot);
		
		g.setColor(Color.GRAY);
		g.fillPolygon(poly);
		g.setColor(Color.BLACK);
		if(screenOrder == 0){
			g.setColor(Color.YELLOW);
		}
		g.drawString(this.text, xLeft + 10, yTop + 10);
		return g;
	}

}
