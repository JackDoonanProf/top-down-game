import java.util.ArrayList;

public class DialogueTree {
	ArrayList<Statement> statementList;
	Statement currentStatement;
	
	public DialogueTree(){
		this.statementList = new ArrayList<Statement>();
	}
	
	public void addStatement(Statement s){
		this.statementList.add(s);
	}
	
	public Statement getStatement(int input){
		if(input == 0){
			return statementList.get(0);
		}
		else{
			int reInt = currentStatement.getResponse(input - 1);
			Statement retStat = statementList.get(reInt);
			currentStatement = retStat;
			return retStat;
		}
		
	}
	
}
