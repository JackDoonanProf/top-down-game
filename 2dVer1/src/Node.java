import java.awt.Point;
import java.util.ArrayList;

public class Node {

	public int xPos;
	public int yPos;
	public String name;
	public ArrayList<Edge> connections;
	
	public Node(int x, int y){
		this.xPos = x;
		this.yPos = y;
		this.name = "Node(" + x +"," + y + ")";
		this.connections = new ArrayList<Edge>();
	}
	
	public void addConnection(Node end){
		Point p1 = new Point(this.xPos,this.yPos);
		Point p2 = new Point(end.xPos,end.yPos);
		double d = p1.distance(p2);
		Edge e = new Edge(this,end,d);
		this.connections.add(e);
	}
	
}
