import java.awt.Color;
import java.util.List;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.geom.Line2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import javax.imageio.ImageIO;

public class Bullet extends Projectile {

	int damage = 1;
	static BufferedImage funt;
	
	public Bullet(int x, int y, String n, Point[] v, Color c, double angle, double velocity) {
		super(x, y, n, v, c, funt, angle, velocity);
		try {
			funt = ImageIO.read(new File("bullet.png"));
		} catch (IOException e) {
		}
		this.texture = funt;
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public String update(ArrayList<Entity> entList,ArrayList<Character> charList,ArrayList<Door> doorList){
		String command = "";
		double previousYPos = this.yPos;
		double previousXPos = this.xPos;
		this.yPos -= Math.cos(Math.toRadians(direction))*velocity;
		this.xPos += Math.sin(Math.toRadians(direction))*velocity;
		
		/*
		for(Character c : charList){
			Point bp = new Point((int)xPos,(int)yPos);
			Polygon charpoly = new Polygon();
			for(int i = 0; i < c.vertexes.length;i++){
				charpoly.addPoint((int)(c.vertexes[i].x+c.xPos),(int)(c.vertexes[i].y+c.yPos));
			}
			if(charpoly.contains(bp)){
				c.Damage(damage);
				this.hit = true;
				break;
			}
		}*/
			Character charHit = doesLineIntersectCharacter(new Point((int)previousXPos,(int)previousYPos),new Point((int)this.xPos,(int)this.yPos),charList);
			if(charHit != null){
				charHit.Damage(damage);
				this.hit = true;
			}
			Entity entityHit = doesLineCutEntity(new Point((int)previousXPos,(int)previousYPos),new Point((int)this.xPos,(int)this.yPos),entList);
			if(entityHit != null){
				//entityHit.(damage);
				this.hit = true;	
			}
		
		return command;
	}
	
	public Entity doesLineCutEntity(Point p1, Point p2,ArrayList<Entity> entityList){
		Entity ent = null;
		Line2D edge = new Line2D.Float(p1,p2);
		for(Entity e:entityList){
			Point[] ev = e.getTranslatedVertexes();
			for(int i = 0; i < ev.length;i++){
				Line2D.Float l2;
				if(i == ev.length - 1){
					l2 = new Line2D.Float(ev[i],ev[0]);
				}
				else{
					l2 = new Line2D.Float(ev[i],ev[i+1]);
				}
				if(l2.intersectsLine(edge)){
					ent = e;
					//System.out.println("COULDNT MAKE EDGE" + edge);
				}
			}
		}
		return ent;
	}
	
	public Character doesLineIntersectCharacter(Point pointA, Point pointB,ArrayList<Character> charList){
		Character cha = null;
		for(Character ch:charList){
			Point center = new Point((int)ch.playerCirc.getCenterX(),(int)ch.playerCirc.getCenterY());
			double radius = ch.playerCirc.getWidth()/2;
			List<Point> answer;
			 double baX = pointB.x - pointA.x;
	        double baY = pointB.y - pointA.y;
	        double caX = center.x - pointA.x;
	        double caY = center.y - pointA.y;

	        double a = baX * baX + baY * baY;
	        double bBy2 = baX * caX + baY * caY;
	        double c = caX * caX + caY * caY - radius * radius;

	        double pBy2 = bBy2 / a;
	        double q = c / a;

	        double disc = pBy2 * pBy2 - q;
		        if (disc < 0) {
		            continue;
		        }
		     // if disc == 0 ... dealt with later
		        double tmpSqrt = Math.sqrt(disc);
		        double abScalingFactor1 = -pBy2 + tmpSqrt;
		        double abScalingFactor2 = -pBy2 - tmpSqrt;

		        Point p1 = new Point((int)(pointA.x - baX * abScalingFactor1),(int)( pointA.y
		                - baY * abScalingFactor1));
		        if (disc == 0) { // abScalingFactor1 == abScalingFactor2
		        	answer = Collections.singletonList(p1);
		        }
		        Point p2 = new Point((int)(pointA.x - baX * abScalingFactor2),(int)( pointA.y
		                - baY * abScalingFactor2));
		        answer = Arrays.asList(p1, p2);
		        
				Line2D edge = new Line2D.Float(pointA,pointB);
		        for(Point collisionPoint : answer){
		        	if(edge.contains(collisionPoint)){
		        		return ch;
		        	}
		        }
		}
		
		return cha;
	}

}
