import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

public class Player extends Character {
	
	Ellipse2D.Float playerCirc;
	
	Sprite sprite;

	
	public Player(int x,int y, String n, String filename,int w, int h){
		super(x,y,n,(filename +".png"));
		this.vertexes[0].x = 0;
		this.vertexes[0].y = 0;
		
		this.vertexes[1].x = w;
		this.vertexes[1].y = 0;

		this.vertexes[2].x = w;
		this.vertexes[2].y = h;
		
		this.vertexes[3].x = 0;
		this.vertexes[3].y = h;
		
		this.originalVertexes = vertexes;
		this.halfX = w/2;
		this.halfY = h/2;
		
		this.collideable = true;
		playerCirc = new Ellipse2D.Float();
		playerCirc.setFrameFromDiagonal(this.vertexes[0], this.vertexes[2]);
		playerCirc.x = (float) this.centre.x;
		playerCirc.y = (float) this.centre.y;
		this.sprite = new Sprite(filename);
	}

	public Command useEquippedItem(Point mouse){
		
		//this.sprite.currentAnimation = this.sprite.animations.get("default");
		//this.sprite.currentAnimation.startAnimation();
		
		double ang = this.calculateRotationFromMouse(mouse, this.centre);
		if(this.equippedItem == null){
			String t = "punch" + ">" + ang + ">" + this.xPos +">" + this.yPos;
			return new Command(t);
		}
		else{
			if(this.equippedItem instanceof Weapon){
				Command com = this.equippedItem.use(mouse,this.xPos,this.yPos);
				com.text = com.text + ">" + ang + ">"+ (this.xPos) +">"+ (this.yPos);
				return com;
			}else{
				Command com = this.equippedItem.use(mouse,this.xPos,this.yPos);
				com.text = com.text + ">" + ang + ">"+ (this.centre.x) +">"+ (this.centre.y);
				return com;		
			}
		}
		
	}
	
	public Command reload(){
		Command c = new Command("");
		if(this.equippedItem instanceof Weapon){
			Weapon tempWeapon = (Weapon)this.equippedItem;
			int currentAmmo = tempWeapon.loadedAmmunition;
			int currentClipSize = tempWeapon.clipSize;
			String projectileName = tempWeapon.proj.name;
			for(Item i : this.inv.contents){
				if(i.name.equals("box of "+ projectileName)){
					AmmoBox tempBox = (AmmoBox)i;
					if(tempBox.contents <= (currentClipSize - currentAmmo)){
						currentAmmo += tempBox.contents;
						tempBox.contents = 0;
					}
					else{
						tempBox.contents -= (currentClipSize - currentAmmo);
						currentAmmo = currentClipSize;
					}
					i = tempBox;
				}
			}
			tempWeapon.loadedAmmunition = currentAmmo;
			this.equippedItem = tempWeapon;
		}
		
		return c;
	}
	
	public int animate(KeybMouse keybstar, Point mousePos,long currentTime){
		int result = this.sprite.animate(currentTime);
		return result;
	}
	public void update(KeybMouse keybstar, Point mousePos){
		long currentTime = GameClock.getInstance().currentTime;
		
		if(this.equippedArmour != null){
			this.updateArmourTexture(this.equippedArmour.wearingFilename);	
			///this.updateTexture(this.equippedArmour.wearingFilename);	
		}
		
		if(keybstar.a){
			rotation = 270;
			xVel = -10;
		}
		if(keybstar.d){
			rotation = 90;
			xVel = +10;
		}
		if(keybstar.w){
			rotation = 0;
			yVel = -10;
		}
		if(keybstar.s){
			rotation = 180;
			yVel = +10;
		}
		
		/*
		switch(buttonClicked){
		case 37: xVel = -10;
				break;
		case 39: xVel = +10;
		break;
		case 38: yVel = -10;
		break;
		case 40: yVel = 10;
		break;
		}*/
		previousXPos = xPos;
		previousYPos = yPos;

		xPos += xVel;
		yPos += yVel;
		collisionsYVel = (int)yVel;
		collisionsXVel = (int)xVel;
		if(this.sprite.animated){
		if(xVel != 0 || yVel != 0){
			
			if(this.sprite.currentAnimation == null){
				this.sprite.currentAnimation = this.sprite.animations.get("run");
				this.sprite.currentAnimation.startAnimation();
			}
			if(this.sprite.currentAnimation.name.compareTo("run") != 0){
				this.sprite.currentAnimation = this.sprite.animations.get("run");
				this.sprite.currentAnimation.startAnimation();
			}
			if(this.sprite.currentAnimation.name.compareTo("run") == 0){
				if(!this.sprite.currentAnimation.playing){
					this.sprite.currentAnimation.startAnimation();
				}
			}
		}
		else{
			if(this.sprite.currentAnimation != null){
				if(this.sprite.currentAnimation.name.compareTo("run") == 0){
					this.sprite.currentAnimation.stopAnimation();
				}	
			}
		}
		}
		xVel = 0;
		yVel = 0;
		//rotation = (calculateRotationFromMouse(mousePos,centre));
		if(this.sprite.animated){
			int currentFrame = this.animate(keybstar,mousePos,currentTime);
			this.texture = this.sprite.getFrame(currentFrame);
		}else{
			this.texture = this.sprite.getSheet();
		}
		
		
		int yTriangle = -(int) (Math.cos( Math.toRadians((this.rotation - 10))) * 200);
		int xTriangle = (int) (Math.sin( Math.toRadians((this.rotation - 10))) * 200);
				
		this.centre.x = (int)this.xPos;
		this.centre.y = (int)this.yPos;

	}
	
	

	
	public double calculateRotationFromMouse(Point mousePos, Point centre){
		double rot = 0;
		int relativeX = mousePos.x - centre.x;
		int relativeY = centre.y - mousePos.y;
		//System.out.println("cX" + centre.x + "mY"+ centre.y);
		//System.out.println("rel X" + relativeX + "relY"+ relativeY);
		//Q1
		if(relativeX > 0 && relativeY > 0){
			double angle = Math.atan((double)relativeX/(double)relativeY);
			angle = Math.toDegrees(angle);
			rot = angle;
		}
		//Q2
		if(relativeX > 0 && relativeY < 0){
			double angle = Math.atan((double)-relativeY/(double)relativeX);
			angle = Math.toDegrees(angle);
			rot = angle + 90;
		}
		//Q3
		if(relativeX < 0 && relativeY < 0){
			double angle = Math.atan((double)-relativeX/(double)-relativeY);
			angle = Math.toDegrees(angle);
			rot = angle + 180;
		}
		//Q4
		if(relativeX < 0 && relativeY > 0){
			double angle = Math.atan((double)relativeY/(double)-relativeX);
			angle = Math.toDegrees(angle);
			rot = angle + 270;
		}
		if(relativeY == 0 && relativeX >= 0){
			return 90;
		}
		if(relativeY == 0 && relativeX < 0){
			return 270;
		}
		if(relativeX == 0 && relativeY >= 0){
			return 180;
		}
		if(relativeX == 0 && relativeY < 0){
			return 0;
		}
		return rot;
		
	}
	
	public Graphics draw(Graphics g, GameLoopTest.GamePanel gp, Camera c){
		//centre = findCentre(vertexes);
		centre = new Point((int)playerCirc.x,(int)playerCirc.y);
		if(texture!= null){
			if(rotation >= 360){
				rotation = rotation - 360;
			}
			AffineTransform identity = new AffineTransform();

			Graphics2D g2d = (Graphics2D)g;
			AffineTransform trans = new AffineTransform();
			trans.setTransform(identity);
			
			trans.translate(xPos, yPos);
			trans.translate(-c.x, -c.y);
			trans.rotate( Math.toRadians(rotation) );
			double halfX = Math.abs(vertexes[0].x - vertexes[1].x)/2;
			double halfY = Math.abs(vertexes[0].y - vertexes[2].y)/2;
			trans.translate(-halfY,-halfX);
			//System.out.println(xPos);
			//System.out.println(yPos);
			
			//System.out.println(trans);
			//ADD SCALING?
			if(scale != 1){
				texture = (BufferedImage)texture.getScaledInstance((int)(scale*(halfX*2)),(int)( scale*(halfY*2)), Image.SCALE_FAST);
			}
			
			
			g2d.drawImage((Image)texture, trans, gp);
			if(armourTexture != null){
				g2d.drawImage((Image)armourTexture, trans, gp);				
			}
			g2d.setColor(Color.BLACK);
			g2d.drawRect((int)this.xPos,(int) this.yPos, 1, 1);
			g2d.setColor(Color.RED);
			this.playerCirc.x -= c.x;
			this.playerCirc.y -= c.y;
			g2d.draw(this.playerCirc);
			this.playerCirc.x += c.x;
			this.playerCirc.y += c.y;

			return g;
		}
		return g;
	}
}
