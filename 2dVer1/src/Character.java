import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Iterator;

import javax.imageio.ImageIO;
import org.json.simple.*;
import org.json.simple.parser.JSONParser;

public class Character extends Entity {
	
	Inventory inv;
	
	int health = 5;
	
	BufferedImage armourTexture;
	
	Item equippedItem;
	Armour equippedArmour;

	Weapon equippedWeapon;
	
	DialogueTree dialogue;

	Ellipse2D.Float playerCirc;
	
	Brain brain;
	
	int collisionsYVel = 0;
	int collisionsXVel = 0;
	
	double previousXPos = 0;
	double previousYPos = 0;
	
	//boolean seesChar = false;
	
	public Character(int x,int y, String n, String filename){
		super(x,y,n,filename);
		inv = new Inventory(10);
		String dialogueFileName = this.name + "dialogue" + ".json";
				
		dialogue = loadDialogue(dialogueFileName);
		
		playerCirc = new Ellipse2D.Float();
		playerCirc.setFrameFromDiagonal(this.vertexes[0], this.vertexes[2]);
		playerCirc.x = (float) this.centre.x;
		playerCirc.y = (float) this.centre.y;
		
		this.brain = new Brain(new Point((int)this.xPos,(int)this.yPos),this.rotation);
	}
	
	public Point findCentre(Point[] ver){
		Point res = new Point();
		if(this.playerCirc == null)return res;
		res.x = (int)(this.playerCirc.getCenterX() + this.xPos);
		res.y = (int)(this.playerCirc.getCenterY() + this.yPos);
		return res;
	}
	
	public Graphics draw(Graphics g, GameLoopTest.GamePanel gp, Camera c){
		centre = findCentre(vertexes);
		if(texture!= null){
			if(rotation >= 360){
				rotation = rotation - 360;
			}
			AffineTransform identity = new AffineTransform();

			Graphics2D g2d = (Graphics2D)g;
			AffineTransform trans = new AffineTransform();
			trans.setTransform(identity);
			
			trans.translate(xPos, yPos);
			trans.translate(-c.x, -c.y);
			trans.rotate( Math.toRadians(rotation) );
			double halfX = Math.abs(vertexes[0].x - vertexes[1].x)/2;
			double halfY = Math.abs(vertexes[0].y - vertexes[2].y)/2;
			trans.translate(-halfY,-halfX);
			//System.out.println(xPos);
			//System.out.println(yPos);
			
			//System.out.println(trans);
			//ADD SCALING?
			if(scale != 1){
				texture = (BufferedImage)texture.getScaledInstance((int)(scale*(halfX*2)),(int)( scale*(halfY*2)), Image.SCALE_FAST);
			}
			
			g2d.drawImage((Image)texture, trans, gp);
			if(armourTexture != null){
				g2d.drawImage((Image)armourTexture, trans, gp);				
			}
			g2d.drawRect((int)this.xPos,(int) this.yPos, 1, 1);
			g2d.setColor(Color.RED);
			this.playerCirc.x -= c.x;
			this.playerCirc.y -= c.y;
			g2d.draw(this.playerCirc);
			this.playerCirc.x += c.x;
			this.playerCirc.y += c.y;
			if(brain.visionCone != null){
				brain.visionCone.translate(-c.x,-c.y);
				if(this.brain.seesChar){
					g2d.setColor(Color.GREEN);
				}
				//g2d.fill(brain.visionCone);
				brain.visionCone.translate(+c.x,+c.y);
			}
			return g;
		}
		return g;
	}
	
	public void updateArmourTexture(String s){
		s = s + ".png";
		BufferedImage img = null;
		try {
		    img = ImageIO.read(new File(s));
		    this.armourTexture = img;
		} catch (IOException e) {
		}
	}
	public void update(KeybMouse keybstar){
	
		if(this.equippedArmour != null){
			this.updateArmourTexture(this.equippedArmour.wearingFilename);	
			///this.updateTexture(this.equippedArmour.wearingFilename);	
		}
		if(this.equippedItem != null){
			if(this.equippedItem.name.compareTo("gun") == 0){
				this.updateTexture("genericCharPistol.png");	
				///this.updateTexture(this.equippedArmour.wearingFilename);	
			}
		}
		this.clickState = calculateLeftClickState(new Point(keybstar.xCoord, keybstar.yCoord),keybstar.lmb);
						
		this.playerCirc.x = (int)this.xPos - (this.playerCirc.width/2);
		this.playerCirc.y = (int)this.yPos - (this.playerCirc.width/2);
		
		this.xVel = 0;
		this.yVel = 0;
		
		previousXPos = xPos;
		previousYPos = yPos;
		
		this.brain.update(new Point((int)this.xPos,(int)this.yPos),this.rotation);
		
		while(!this.brain.actionQueue.isEmpty()){
			String action = this.brain.actionQueue.remove();
			parseActionString(action);
		}
		this.xPos += this.xVel;
		this.yPos += this.yVel;
		this.rotation = Math.abs(this.rotation);
		if(this.rotation >= 360){
			this.rotation = this.rotation - 360;
		}
		
		/*
		GameLoopTest glt = GameLoopTest.getInstance();
		this.seesChar = false;
		for(Character c: glt.charList){
			//System.out.println(c.name);
			if(visionCone.contains(c.centre)){
				this.seesChar = true;
			}
		}
		Point playCentre = new Point((int)glt.play.xPos,(int)glt.play.yPos);
		if(visionCone.contains(playCentre)){
			this.seesChar = true;
		}*/
	}
	
	public void parseActionString(String action){
		String[] splitAction = action.split(",");
		
		if(splitAction[0].compareTo("face") == 0){
			double angleToTurnTo = Double.parseDouble(splitAction[1]);
			this.rotation = angleToTurnTo;
		}
		
		if(splitAction[0].compareTo("turnto") == 0){
			double angleToTurnTo = Double.parseDouble(splitAction[1]);
			double difference = angleToTurnTo - this.rotation;
			System.out.println("angle:" + difference);
			int speedToTurn = Integer.parseInt(splitAction[3]);
			if(angleToTurnTo > this.rotation){
				this.rotation += speedToTurn;
			}else{
				this.rotation -= speedToTurn;
			}
		}
		if(splitAction[0].compareTo("move") == 0){
			double xSpeed = Double.parseDouble(splitAction[1]);
			double ySpeed = Double.parseDouble(splitAction[2]);
			this.xVel = xSpeed;
			this.yVel = ySpeed;
		}
		if(splitAction[0].compareTo("fireWeapon") == 0){
			if(this.equippedItem instanceof Weapon){
				Weapon tempWep = (Weapon)this.equippedItem;
				int x = Integer.parseInt(splitAction[1]);
				int y = Integer.parseInt(splitAction[2]);
				String angle = splitAction[3];
				tempWep.loadedAmmunition = 10;
				
				String com = tempWep.use(new Point(x,y), this.xPos, this.yPos).text;				
				com = com + ">" + angle + ">"+ (this.xPos) +">"+ (this.yPos);
				GameLoopTest.getInstance().processGameCommand(com);
			}
		}
		if(splitAction[0].compareTo("complete") == 0){
			String behaviourToMoveTo = splitAction[2];
			brain.startBehaviour(behaviourToMoveTo);
		}

		
	}
	public void Equip(Item i){
		this.equippedItem = i;
	}	
	
	public void EquipWeapon(Weapon w){
		this.equippedWeapon = w;
	}
	
	public void unEquip(){
		this.equippedWeapon = null;
	}
	
	public void Damage(int d){
		this.health -= d;
	}
	
	public Command useEquippedItem(){
		return equippedItem.use(null,this.xPos,this.yPos);
	}
	
	public Command dialogueClick(){
		return new Command("dialogue>start>" + this.name);
	}
	
	public Entity collisionCheck(Entity e){
		//playerCirc.setFrameFromDiagonal(this.vertexes[0], this.vertexes[2]);
		
		playerCirc.x = (int)(this.xPos - this.halfX);
		playerCirc.y = (int)(this.yPos - this.halfY);
		//Line2D.Float l1 = new Line2D.Float(e.centre,this.centre);
		
		//VERTICAL LINE
		Line2D.Float lu = new Line2D.Float(this.centre,new Point(this.centre.x,(int)(this.centre.y - (this.halfY * 2))));
		//DOWN LINE
		Line2D.Float ld = new Line2D.Float(this.centre,new Point(this.centre.x,(int)(this.centre.y + (this.halfY * 2))));

		//RIGHT LINE
		Line2D.Float lr = new Line2D.Float(this.centre,new Point((int)(this.centre.x + (this.halfX * 2)),this.centre.y));
		//RIGHT LINE
		Line2D.Float ll = new Line2D.Float(this.centre,new Point((int)(this.centre.x - (this.halfX * 2)),this.centre.y));
				
		Point[] ev = e.getTranslatedVertexes();
		for(int i = 0; i < ev.length;i++){
			Line2D.Float l2;
			if(i == ev.length - 1){
				l2 = new Line2D.Float(ev[i],ev[0]);
			}
			else{
				l2 = new Line2D.Float(ev[i],ev[i+1]);
			}
			
			if(l2.intersectsLine(lu)){
				Point inters = intersection(lu,l2);
				if(inters != null){
					if(inters.getY() > this.yPos - this.halfY){
						System.out.println("COLLISION");
						this.yPos = inters.y + 2 + this.halfY;
						if(e.moveable){
							e.yPos += this.collisionsYVel;
						}
						return e;
					}
				}
			}
			if(l2.intersectsLine(ld)){
				Point inters = intersection(ld,l2);
				if(inters != null){
					if(inters.getY() < this.yPos + (this.halfY)){
						System.out.println("COLLISION");
						this.yPos = inters.y - 2 - this.halfY;
						if(e.moveable){
							e.yPos += this.collisionsYVel;
						}
						return e;
					}	
				}
			}
			if(l2.intersectsLine(lr)){
				Point inters = intersection(lr,l2);
				if(inters!= null){
					if(inters.getX() < this.xPos + (this.halfX)){
						System.out.println("COLLISION");
						this.xPos = inters.x - 2 - (this.halfX);
						if(e.moveable){
							e.xPos += this.collisionsXVel;
						}
						return e;
					}					
				}
			}
			if(l2.intersectsLine(ll)){
				Point inters = intersection(ll,l2);
				if(inters != null){
					if(inters.getX() > this.xPos - this.halfX){
						System.out.println("COLLISION");
						this.xPos = inters.x + 2 +this.halfX;
						if(e.moveable){
							e.xPos += this.collisionsXVel;
						}
						return e;
					}					
				}
			}
		}
		//DO CORNERS TOO
		for(int i = 0; i < ev.length;i++){
			if(ev[i].distance(this.centre) <= this.halfX){
				this.xPos -= this.xVel;
				this.yPos -= this.yVel;
			}
		}
		
		return e;
	}
	
	public void rayCollisionCheck(){
		Entity e = doesLineCutEntity(new Point((int)this.xPos,(int)this.yPos),new Point((int)this.previousXPos,(int)this.previousYPos),GameLoopTest.getInstance().entityList);
		if(e != null){
			this.xPos = this.previousXPos;
			this.yPos = this.previousYPos;
		}
	}
	public Entity doesLineCutEntity(Point p1, Point p2,ArrayList<Entity> entityList){
		Entity ent = null;
		Line2D edge = new Line2D.Float(p1,p2);
		for(Entity e:entityList){
			Point[] ev = e.getTranslatedVertexes();
			for(int i = 0; i < ev.length;i++){
				Line2D.Float l2;
				if(i == ev.length - 1){
					l2 = new Line2D.Float(ev[i],ev[0]);
				}
				else{
					l2 = new Line2D.Float(ev[i],ev[i+1]);
				}
				if(l2.intersectsLine(edge)){
					ent = e;
					//System.out.println("COULDNT MAKE EDGE" + edge);
				}
			}
		}
		return ent;
	}
	
	public Point intersection(Line2D l1, Line2D l2){

		int d = (int) ( (l1.getX1() - l1.getX2())*(l2.getY1() - l2.getY2()) - ( (l1.getY1() - l1.getY2())*(l2.getX1() - l2.getX2()) ) );
		if(d == 0){
			return null;
		}		
		int xi = (int) ( ( (  (l2.getX1() - l2.getX2())*( l1.getX1()*l1.getY2() -l1.getY1()*l1.getX2() )  ) - ( ( l1.getX1() - l1.getX2() )*( l2.getX1()*l2.getY2() - l2.getY1()*l2.getX2()) )  )/d);
		
		
		int yi = (int) ( ( (  (l2.getY1() - l2.getY2())*( l1.getX1()*l1.getY2() -l1.getY1()*l1.getX2() )  ) - ( ( l1.getY1() - l1.getY2() )*( l2.getX1()*l2.getY2() - l2.getY1()*l2.getX2()) )  )/d);

		
		return new Point(xi,yi);
		
	}
	
	public DialogueTree loadDialogue(String filename) {
		DialogueTree d = new DialogueTree();
		
		JSONParser parser = new JSONParser();

        try {

            Object obj = parser.parse(new FileReader(filename));

            JSONObject jsonObject = (JSONObject) obj;
            // loop array
            JSONArray statements = (JSONArray)jsonObject.get("statements");
            Statement f = new Statement("");
            for(Object j : statements){
            	JSONObject jo = (JSONObject)j;
            	f = new Statement((String)jo.get("text"));
            	JSONArray events = (JSONArray)jo.get("events");
            	for(Object e: events){
            		f.addEvent(e.toString());
            	}
            	JSONArray responses = (JSONArray)jo.get("responses");
            	for(Object r: responses){
            		JSONObject jr = (JSONObject)r;
            		String rText = jr.get("text").toString();
            		int GOTO = Integer.parseInt(jr.get("GOTO").toString());
            		JSONArray rEvents = (JSONArray)jr.get("events");
            		Response res = new Response(rText);
            		res.GOTO = GOTO;
            		for(Object e: rEvents){
                		res.addTrigger(e.toString());
                	}
            		f.addResponse(res);
            	}
            	d.addStatement(f);
            }
          

        } catch (Exception e) {
            e.printStackTrace();
        }
		return d;
	}
	
	@Override
	public Command doClick(){
		System.out.println("BRBRRAAAPPPPP");
		return new Command("");
	}
}
