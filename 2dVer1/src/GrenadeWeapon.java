import java.awt.Color;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class GrenadeWeapon extends Weapon {

	double firingVelocity;
	
	public GrenadeWeapon(String n) {
		super(n);
		Point p1 = new Point(0,0);
		Point p2 = new Point(0,20);
		Point p3 = new Point(20,20);
		Point p4 = new Point(20,0);
		Point[] points = new Point[4];
		points[0] = p1;
		points[1] = p2;
		points[2] = p3;
		points[3] = p4;
		this.proj = new Projectile(0,0,"Grenade",points,Color.BLUE,null,0,2);
		this.firingVelocity = 20;
		this.clipSize = 1;
		this.loadedAmmunition = 0;
		BufferedImage funt = null;
		iconFilename = "gunStandardIcon.png";
		try {
			funt = ImageIO.read(new File("gunStandardIcon.png"));
			
		} catch (IOException e) {
		}
		this.icon = funt;
	}
	
	@Override
	public Command use(Point mouse,double x, double y){
		double dist = mouse.distance(new Point((int)x,(int)y));
		if(dist > 600)dist = 600;
		firingVelocity = dist/25;
		return new Command("throw" + ">" + proj.name + ">" + firingVelocity);
	}

}
