import java.awt.Point;

public class Item {
	public String name;
	public String iconFilename;
	public Item(String n){
		this.name = n;
		this.iconFilename = "defaultIcon.png";
	}
	
	public Command use(Point mouse, double x, double y){
		return new Command("");
	}

}
