import java.util.ArrayList;

public class Response {
	String text;
	ArrayList<String> triggers;
	int GOTO;
	
	public Response(String t){
		this.text = t;
		this.triggers = new ArrayList<String>();
		this.GOTO = 1;
	}
	
	public void addTrigger(String e){
		this.triggers.add(e);
	}
}
