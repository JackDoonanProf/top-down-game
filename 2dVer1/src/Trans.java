import java.awt.Color;
import java.awt.Point;
import java.awt.image.BufferedImage;

public class Trans extends Entity {

	String levelFilename;
	
	public Trans(int x, int y, String n, Point[] v, Color c, String fname) {
		super(x, y, n, v, c, null);
		this.levelFilename = fname;
		// TODO Auto-generated constructor stub
	}
	@Override
	public Command doClick(){
		return new Command(levelFilename);
		
	}
}
