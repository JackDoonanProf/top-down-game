import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Polygon;

public class HUDElement extends Entity {

	String command;
	Item associatedItem;
	int itemIndex;
	Color stateColor;
	String listID;
	//0 = away, 1 = hover, 2 = clickDown, 3 = released
	public HUDElement(int x, int y, String n,String list, int index) {
		super(x, y, n);
		vertexes = new Point[4];
		vertexes[0] = new Point(0,0);
		vertexes[1] = new Point(0,0);
		vertexes[2] = new Point(0,0);
		vertexes[3] = new Point(0,0);
		
		int xLeft = (int)this.xPos ;
		int xRight = xLeft + 40;
		int yTop = (int)this.yPos ;
		int yBot = yTop + 20;
		
		vertexes[0] = new Point(xLeft,yTop);
		vertexes[1] = new Point(xRight,yTop);
		vertexes[2] = new Point(xRight,yBot);
		vertexes[3] = new Point(xLeft,yBot);

		this.itemIndex = index;
		this.listID = list;
		// TODO Auto-generated constructor stub
	}
	public Graphics draw(Graphics g, GameLoopTest.GamePanel gp, Camera c){
		//rotation ++;
		Polygon poly = new Polygon();
		
		if(this.clickState == 0 || this.clickState == 3){
			this.stateColor = Color.GRAY;
		}
		if(this.clickState == 1){
			this.stateColor = Color.BLUE;
		}
		if(this.clickState == 2){
			this.stateColor = Color.RED;
		}

		poly.addPoint(vertexes[0].x-c.x,vertexes[0].y-c.y);
		poly.addPoint(vertexes[1].x-c.x,vertexes[1].y-c.y);
		poly.addPoint(vertexes[2].x-c.x,vertexes[2].y-c.y);
		poly.addPoint(vertexes[3].x-c.x,vertexes[3].y-c.y);
		
		g.setColor(stateColor);
		g.fillPolygon(poly);
		g.setColor(Color.BLACK);
		g.drawString(this.name, vertexes[0].x + 10 -c.x, vertexes[0].y + 10 -c.y);
		return g;
	}
	
	public int calculateLeftClickState(Point mouse,boolean click){
		int lastState = this.clickState;
		int c = 0;
		
		Polygon thispoly = new Polygon();
		for(int i = 0; i < vertexes.length;i++){
			thispoly.addPoint(this.vertexes[i].x ,this.vertexes[i].y);
		}
		System.out.println("MOUSE Y" + mouse.y);
		System.out.println("POLY Y" + thispoly.ypoints[0]);

		if(thispoly.contains(mouse)){
			c = 1;
			if(click){
				c = 2;
			}
		 }
		if(lastState == 2 && c == 1)return 3;
		//releasing away from the object
		//Hovering
		if(c == 1)return c;
		//Clicking Down
		if(c == 2)return c;
		//releasing after clicking on object
		return 0;
	}

}