import java.awt.Point;

public class Sound {
	String name;
	double lifetime;
	double birthday;
	boolean alive;
	int volume;
	Point origin;
	
	public Sound(String n, double l, int v, Point p){
		this.name = n;
		this.lifetime = l;
		this.volume = v;
		this.origin = p;
		this.alive = true;
		this.birthday = GameClock.getInstance().currentTime;
	}
	
	public void update(){
		double cuntTime = GameClock.getInstance().currentTime;
		if(cuntTime - this.birthday > this.lifetime){
			this.alive = false;
		}
	}
	
}