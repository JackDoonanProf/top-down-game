
public class SpriteAnimation {

	int frameRate = 15;
	String name = "default";
	int numFrames = 1;
	int currentFrame = 0;
	
	int relativeFrame = 0;
	
	int[] animationFrames;
	
	boolean playing = false;
	boolean looping = false;
	boolean complete = false;
	
	long startTime = 0;
	
	long lastTime = 0;
	
	long frameDifference = 0;
	
	
	public SpriteAnimation(String n, int num, boolean loop, int framer, int[] frames){
		this.name = n;
		this.numFrames = num;
		this.currentFrame = frames[0];
		this.looping = loop;
		this.animationFrames = frames;
		this.frameRate = framer;
		//FrameDifference is 1000 milliseconds divided by framerate
		// = number of milliseconds to pass till next frame change
		this.frameDifference = 1000/framer;
	}
	
	public void startAnimation(){
		this.startTime = GameClock.getInstance().currentTime;
		this.lastTime = GameClock.getInstance().currentTime;
		this.playing = true;
		this.complete = false;
	}
	public void pauseAnimation(){
		this.playing = false;
	}
	public void endAnimation(){
		this.playing = false;
		this.complete = true;
	}
	public void updateAnimation(long currentTime){
		long timeDiff = currentTime - startTime;
		int framesElapsed =  (int) ((int) timeDiff/this.frameDifference);
		if(framesElapsed >= this.numFrames - 1 && this.looping == false){
			this.relativeFrame = this.numFrames - 1;
			this.currentFrame = this.animationFrames[this.relativeFrame];
			this.complete = true;
			this.playing = false;
			return;
		}
		this.relativeFrame = framesElapsed % this.numFrames;
		
		this.currentFrame = this.animationFrames[this.relativeFrame];
	}

	public void stopAnimation() {
		this.playing = false;
		this.complete = true;
		this.currentFrame = this.animationFrames[this.animationFrames.length - 1];
	}
}
