
public class Armour extends Item {

	
	//Blunt impact like a punch. Big padded stuff will get 
	//high value
	//0 means it will do nothing to hinder that type of damage
	//1 means it stops all of that type of damage
	//.5 means it stops 50% of that type
	double bluntDefence = 0.0;
	
	//Penetration from things like bullets arrows, knifes
	//pointyness value must be above this value to do any damage at all
	
	//For instance,a hammer with a pointyness value of 0 will not be able to do
	//penetration damage.
	//a slightly pointy knife will penetrate a big padded jacket
	//and a high velocity high calibre bullet will penetrate anything
	double penetrationDefence = 0.0;

	String faction = "";
	
	String wearingFilename = "";
	
	public Armour(String n, String filename, String fac, double pen,double blunt) {
		super(n);
		this.faction = fac;
		this.penetrationDefence = pen;
		this.bluntDefence = blunt;
		this.wearingFilename = filename;
		// TODO Auto-generated constructor stub
	}

}
