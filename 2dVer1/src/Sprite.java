import java.awt.Color;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.imageio.ImageIO;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class Sprite {

	BufferedImage sheet;
	ArrayList<Rectangle> frames;
	boolean animated = false;
	HashMap<String,SpriteAnimation> animations;
	
	SpriteAnimation currentAnimation;
	
	public Sprite(String filename){
		sheet = null;
		frames = new ArrayList<Rectangle>();
		animations = new HashMap<String,SpriteAnimation>();
		try {
			sheet = ImageIO.read(new File(filename + ".png"));
		} catch (IOException e) {
			System.err.println("SPRITE PNG NOT FOUND");
		}
		
		filename = filename + ".json";
		JSONParser parser = new JSONParser();
		try {

            Object obj = parser.parse(new FileReader(filename));

            JSONObject jsonObject = (JSONObject) obj;
            
			//SETUP Frames
			JSONArray framesJSON = (JSONArray) jsonObject.get("Frames");
			for(Object FJSON: framesJSON){
				JSONObject jo = (JSONObject)FJSON;
				String name  = jo.get("name").toString();
				
				JSONArray pos  = (JSONArray)jo.get("position");
				int posX = Integer.parseInt(pos.get(0).toString());
				int posY = Integer.parseInt(pos.get(1).toString());
				
				int width = Integer.parseInt(jo.get("w").toString());
				int height = Integer.parseInt(jo.get("h").toString());
				
				Rectangle r = new Rectangle();
				r.setBounds(posX, posY, width, height);
				frames.add(r);
			}
			//Get animations
			JSONArray animssJSON = (JSONArray) jsonObject.get("Animations");
			for(Object AJSON: animssJSON){
				JSONObject jo = (JSONObject)AJSON;
				String name  = jo.get("name").toString();
								
				int rate = Integer.parseInt(jo.get("framerate").toString());
				boolean loop = Boolean.parseBoolean(jo.get("looping").toString());
				
				boolean consecutive = Boolean.parseBoolean(jo.get("consecutive").toString());

				SpriteAnimation sa;

				if(consecutive){
					int start = Integer.parseInt(jo.get("startFrame").toString());
					int num = Integer.parseInt(jo.get("numFrames").toString());
					int[] frameArray = new int[num];
					for(int i = 0; i < (num); i ++){
						frameArray[i] = start + i;
					}
					sa = new SpriteAnimation(name,num,loop,rate,frameArray);
				}
				else{
					JSONArray frms  = (JSONArray)jo.get("frames");
					int[] frameArray = new int[frms.size()];
					for(int i = 0; i < frms.size(); i ++){
						frameArray[i] = Integer.parseInt(frms.get(i).toString());
					}
					sa = new SpriteAnimation(name,frms.size(),loop,rate,frameArray);
				}
				animations.put(name, sa);
				this.animated = true;
			}
			
		} catch (Exception e) {
			//e.printStackTrace();
			this.animated = false;
		}
	}
	
	public BufferedImage getSheet(){
		return this.sheet;
	}
	public int animate(long current){
		if(this.currentAnimation != null){
			if(!this.currentAnimation.complete && this.currentAnimation.playing){
				this.currentAnimation.updateAnimation(current);
				return currentAnimation.currentFrame;
			}else{
				return currentAnimation.currentFrame;
			}
		}else{
			return 0;
		}
	}
	
	public BufferedImage getFrame(int frameNumber){
		BufferedImage result = null;
		if(frameNumber > this.frames.size()){
			System.err.println("COULDNT FIND FRAME: " + frameNumber);
			return result;
		}
		Rectangle _frame = frames.get(frameNumber);
		
		result = this.sheet.getSubimage(_frame.x,_frame.y,_frame.width,_frame.height);
		
		return result;
	}
}
