import java.awt.image.BufferedImage;

public class Weapon extends Item {
	int loadedAmmunition;
	int clipSize;
	Projectile proj;
	BufferedImage icon;	
	Long lastTimeFired = 0l;
	Long fireInterval = 1000l;
	public Weapon(String n) {
		super(n);
		this.clipSize = 10;
		this.loadedAmmunition = 0;
		this.iconFilename = "defaultWeaponIcon";
		// TODO Auto-generated constructor stub
	}
	
	public void reload(int numBullets){
		this.loadedAmmunition += numBullets;
	}
	

}
