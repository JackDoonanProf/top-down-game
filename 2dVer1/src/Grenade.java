import java.awt.Color;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;

public class Grenade extends Projectile {
	//int damage = 1;
	static BufferedImage funt;
	Ellipse2D.Float grenadeCirc;
	public long birthDay;
	int fuseLength;
	static int EXPLOSION_DIAMETER = 100;
	
	public Grenade(int x, int y, String n, Point[] v, Color c, double angle, double velocity) {
		super(x, y, n, v, c, funt, angle, velocity);
		try {
			funt = ImageIO.read(new File("Grenade.png"));
			
		} catch (IOException e) {
		}
		this.texture = funt;
		this.grenadeCirc = new Ellipse2D.Float();
		grenadeCirc.setFrameFromDiagonal(new Point(0,0), new Point(20,20));
		this.centre = this.findCentre(this.getTranslatedVertexes());
		this.birthDay = 0;
		this.fuseLength = 3000;
		// TODO Auto-generated constructor stub
	}
	@Override
	public String update(ArrayList<Entity> entList,ArrayList<Character> charList,ArrayList<Door> doorList){
		String command = "";
		this.halfX = this.halfY;
		grenadeCirc.x = (int)(this.xPos - this.halfX);
		grenadeCirc.y = (int)(this.yPos - this.halfY);
		this.yPos -= Math.cos(Math.toRadians(direction))*velocity;
		this.xPos += Math.sin(Math.toRadians(direction))*velocity;
		velocity -=.5;
		if(velocity <= 0){
			this.velocity = 0;
		}
		
		for(Entity e: entList){
			collisionCheck(e);
		}
		for(Door d : doorList){
			collisionCheck(d);
		}
		
		return command;
	}
	
	public void checkTimer(){
		long timeElapsed = GameClock.getInstance().currentTime;
		if(timeElapsed - this.birthDay >= this.fuseLength){
			this.hit = true;
		}
	}
	
	public String getHitCommand(){
		return "create>explosion>" + (this.xPos - (this.halfX*2)) + ">" + (this.yPos - this.halfY - (EXPLOSION_DIAMETER/2)) + ">" + EXPLOSION_DIAMETER;
	}
	
	public Entity collisionCheck(Entity e){
		//grenadeCirc.setFrameFromDiagonal(this.vertexes[0], this.vertexes[2]);

		//Line2D.Float l1 = new Line2D.Float(e.centre,this.centre);
		
		//VERTICAL LINE
		Line2D.Float lu = new Line2D.Float(this.centre,new Point(this.centre.x,(int)(this.centre.y - (this.halfY * 2))));
		//DOWN LINE
		Line2D.Float ld = new Line2D.Float(this.centre,new Point(this.centre.x,(int)(this.centre.y + (this.halfY * 2))));

		//RIGHT LINE
		Line2D.Float lr = new Line2D.Float(this.centre,new Point((int)(this.centre.x + (this.halfX * 2)),this.centre.y));
		//RIGHT LINE
		Line2D.Float ll = new Line2D.Float(this.centre,new Point((int)(this.centre.x - (this.halfX * 2)),this.centre.y));
				
		Point[] ev = e.getTranslatedVertexes();
		for(int i = 0; i < ev.length;i++){
			Line2D.Float l2;
			if(i == ev.length - 1){
				l2 = new Line2D.Float(ev[i],ev[0]);
			}
			else{
				l2 = new Line2D.Float(ev[i],ev[i+1]);
			}
			
			if(l2.intersectsLine(lu)){
				Point inters = intersection(lu,l2);
				if(inters != null){
					if(inters.getY() > this.yPos - this.halfY){
						System.out.println("COLLISION");
						this.yPos = inters.y + 2 + this.halfY;
						this.direction = 2 * 180 - this.direction - 180;
						return e;
					}
				}
			}
			if(l2.intersectsLine(ld)){
				Point inters = intersection(ld,l2);
				if(inters != null){
					if(inters.getY() < this.yPos + (this.halfY)){
						System.out.println("COLLISION");
						this.yPos = inters.y - 2 - this.halfY;
						this.direction = 2 * 0 - this.direction - 180;
						return e;
					}	
				}
			}
			if(l2.intersectsLine(lr)){
				Point inters = intersection(lr,l2);
				if(inters!= null){
					if(inters.getX() < this.xPos + (this.halfX)){
						System.out.println("COLLISION");
						this.xPos = inters.x - 2 - (this.halfX);
						this.direction = 2 * 270 - this.direction - 180;
						return e;
					}					
				}
			}
			if(l2.intersectsLine(ll)){
				Point inters = intersection(ll,l2);
				if(inters != null){
					if(inters.getX() > this.xPos - this.halfX){
						System.out.println("COLLISION");
						this.xPos = inters.x + 2 +this.halfX;
						this.direction = 2 * 90 - this.direction - 180;
						return e;
					}					
				}
			}
		}
		//DO CORNERS TOO
		for(int i = 0; i < ev.length;i++){
			if(ev[i].distance(this.centre) <= this.halfX){
				this.xPos -= this.xVel;
				this.yPos -= this.yVel;
			}
		}
		
		return e;
	}
	
	public Point intersection(Line2D l1, Line2D l2){

		int d = (int) ( (l1.getX1() - l1.getX2())*(l2.getY1() - l2.getY2()) - ( (l1.getY1() - l1.getY2())*(l2.getX1() - l2.getX2()) ) );
		if(d == 0){
			return null;
		}		
		int xi = (int) ( ( (  (l2.getX1() - l2.getX2())*( l1.getX1()*l1.getY2() -l1.getY1()*l1.getX2() )  ) - ( ( l1.getX1() - l1.getX2() )*( l2.getX1()*l2.getY2() - l2.getY1()*l2.getX2()) )  )/d);
		
		
		int yi = (int) ( ( (  (l2.getY1() - l2.getY2())*( l1.getX1()*l1.getY2() -l1.getY1()*l1.getX2() )  ) - ( ( l1.getY1() - l1.getY2() )*( l2.getX1()*l2.getY2() - l2.getY1()*l2.getX2()) )  )/d);

		
		return new Point(xi,yi);
		
	}
}
