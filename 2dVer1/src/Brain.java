import java.awt.Point;
import java.awt.Polygon;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.PriorityQueue;

import org.json.simple.*;

public class Brain {
	PriorityQueue<String> actionQueue;
	
	ArrayList<CharacterAwareness> charactersAwareOf;
	
	Character hostileTo;
	double distanceToHostileChar = Double.MAX_VALUE;
	boolean seesHostileChar = false;
	
	boolean seesChar;
	Behaviour currentBehaviour = null;
	
	HashMap<String,Behaviour> behaviourMap = new HashMap<String,Behaviour>();
	Polygon visionCone;
	int VISIONCONE_ANGLE = 30;
	int VISIONCONE_DISTANCE = 200;
	
	Point home;

	Point position;
	double rotation;
	
	Point fleeingFrom;
	
	ArrayList<String> hostileFactions = new ArrayList<String>();
	ArrayList<String> friendlyFactions = new ArrayList<String>();
	ArrayList<String> alliedFactions = new ArrayList<String>();
	
	public Brain(Point p,double rot){

		this.position = p;
		this.rotation = rot;
		
		this.actionQueue = new PriorityQueue<String>();
		this.charactersAwareOf = new ArrayList<CharacterAwareness>();
		int yTriangle = (int) (Math.cos(this.rotation - VISIONCONE_ANGLE) * VISIONCONE_DISTANCE);
		int xTriangle = (int) (Math.sin(this.rotation - VISIONCONE_ANGLE) * VISIONCONE_DISTANCE);
		
		this.visionCone = new Polygon();
		visionCone.addPoint(this.position.x,this.position.y);
		visionCone.addPoint(this.position.x + xTriangle,this.position.y + yTriangle);
		
		yTriangle = (int) -(Math.cos(this.rotation + VISIONCONE_ANGLE) * VISIONCONE_DISTANCE);
		xTriangle = (int) (Math.sin(this.rotation + VISIONCONE_ANGLE) * VISIONCONE_DISTANCE);
		
		visionCone.addPoint(this.position.x + xTriangle,this.position.y + yTriangle);
		
	}
	
	public void readBehaviours(JSONArray behaviours){
		for(Object j: behaviours){
			JSONObject jo = (JSONObject)j;
			String n = jo.get("name").toString();
			String onComp = jo.get("onComplete").toString();
			String type = jo.get("type").toString();
			Behaviour b = new Behaviour(n,onComp,type,this);
			if(jo.containsKey("home")){
				String Shome = jo.get("home").toString();
				String Sposition = Shome.substring(1, Shome.length() - 1);
				String[] splitPosition = Sposition.split(",");
				int xGoal = Integer.parseInt(splitPosition[0]);
				int yGoal = Integer.parseInt(splitPosition[1]);
				this.home = new Point(xGoal,yGoal);
			}
			JSONArray jsa = (JSONArray)jo.get("nodes");
			b.readNodeArray(jsa);
			
			this.behaviourMap.put(n, b);				
		}
		Behaviour h = new Behaviour("hostile","default","hostile",this);
		this.behaviourMap.put("hostile", h);				
		this.currentBehaviour = behaviourMap.get("default");	
	}
	
	public void startBehaviour(String b){
		if(behaviourMap.containsKey(b)){
			this.currentBehaviour = behaviourMap.get(b);
			this.currentBehaviour.currentNode = 0;
			this.currentBehaviour.reset();
		}
		else{
			System.err.println("CANT FIND BEHAVIOUR");
		}
	}
	
	public void calculateActionsFromBehaviours(Point p,double rot){
		if(this.currentBehaviour != null){
			
			
			if(this.currentBehaviour.type.compareTo("hostile") == 0){
				this.VISIONCONE_ANGLE = 45;
				this.VISIONCONE_DISTANCE = 800;
				if(this.seesHostileChar){
					double angleToHostileChar = this.calculateRotationFromMouse(this.hostileTo.centre, this.position);
					boolean turn = true;
					if(angleToHostileChar > this.rotation){
						double x = 0;
						double theta = this.rotation + (360 - angleToHostileChar);
						theta = theta%360;
						if((theta < 5) ){
							turn = false;
						}else
						{
							if(angleToHostileChar - this.rotation > 30){
								actionQueue.add("turnto," + angleToHostileChar + ",speed,5");
								return;
							}
							else{
								actionQueue.add("turnto," + angleToHostileChar + ",speed,2");		
								return;
							}
						}
						
					}
					else{
						if((this.rotation - angleToHostileChar < 5) ){
							turn = false;
						}else
						{
							if(angleToHostileChar - this.rotation > 30){
								actionQueue.add("turnto," + angleToHostileChar + ",speed,5");
								return;
							}
							else{
								actionQueue.add("turnto," + angleToHostileChar + ",speed,2");
								return;
							}
						}	
					}
					double distanceToChar = this.position.distance(this.hostileTo.centre);
					if(distanceToChar > 250){
						int xGoal = this.hostileTo.centre.x;
						int yGoal = this.hostileTo.centre.y;

						double angletoTurn = calculateRotationFromMouse(new Point(xGoal,yGoal),this.position);
						System.out.println("Comparing:" + angletoTurn + " to" + this.rotation);
						if(this.position.distance( new Point(xGoal,yGoal)) < 5 ){
							double angleToSend = calculateRotationFromMouse(new Point(xGoal,yGoal),this.position);
							double xVelToSend = 1 * Math.sin(Math.toRadians(angleToSend));
							double yVelToSend = 1 * Math.cos(Math.toRadians(angleToSend));
							actionQueue.add("move," + xVelToSend + "," + -yVelToSend);
							return;
						}
						if(this.position.distance( new Point(xGoal,yGoal)) < 20 ){
							double angleToSend = calculateRotationFromMouse(new Point(xGoal,yGoal),this.position);
							double xVelToSend = 3 * Math.sin(Math.toRadians(angleToSend));
							double yVelToSend = 3 * Math.cos(Math.toRadians(angleToSend));
							actionQueue.add("move," + xVelToSend + "," + -yVelToSend);
							return;
						}
						else{
							double angleToSend = calculateRotationFromMouse(new Point(xGoal,yGoal),this.position);
							double xVelToSend = 5 * Math.sin(Math.toRadians(angleToSend));
							double yVelToSend = 5 * Math.cos(Math.toRadians(angleToSend));
							actionQueue.add("move," + xVelToSend + "," + -yVelToSend);
							return;
						}
					}
					
					//WE DONT HAVE TO TURN SO WE CAN START SHOOTING
					
					actionQueue.add("fireWeapon," + (int)this.hostileTo.centre.x +"," + (int)this.hostileTo.centre.y +","+ angleToHostileChar);
					
				}
				return;
			}
			
			String res = this.currentBehaviour.updateNode(p,rot);
			actionQueue.add(res);
		}
	}
	
	public void update(Point p,double rot){
		this.position = p;
		this.rotation = rot;
		
		this.visionCone = new Polygon();
		
		int yTriangle = (int) (Math.cos( Math.toRadians(this.rotation - VISIONCONE_ANGLE)) * VISIONCONE_DISTANCE);		
		int xTriangle = (int) (Math.sin( Math.toRadians(this.rotation - VISIONCONE_ANGLE)) * VISIONCONE_DISTANCE);

		visionCone.addPoint((int)this.position.x,(int)this.position.y);
		visionCone.addPoint((int)this.position.x + xTriangle,(int)this.position.y - yTriangle);
		
		yTriangle = (int) (Math.cos(Math.toRadians(this.rotation + VISIONCONE_ANGLE)) * VISIONCONE_DISTANCE);
		xTriangle = (int) (Math.sin(Math.toRadians(this.rotation + VISIONCONE_ANGLE)) * VISIONCONE_DISTANCE);
		
		visionCone.addPoint((int)this.position.x + xTriangle,(int)this.position.y - yTriangle);

		
		GameLoopTest glt = GameLoopTest.getInstance();
		this.seesChar = false;
		for(Character c: glt.charList){
			//System.out.println(c.name);
			if(visionCone.contains(c.centre)){
				this.seesChar = true;
			}
		}
		Point playCentre = new Point((int)glt.play.xPos,(int)glt.play.yPos);
		if(visionCone.contains(playCentre)){
			this.seesChar = true;
			if(this.currentBehaviour != null && this.currentBehaviour.type.compareTo("hostile") !=0){
				if(glt.play.equippedArmour != null){
					if(!this.alliedFactions.contains(glt.play.equippedArmour.faction)){
						this.hostileTo = glt.play;
						this.seesHostileChar = true;
						if(this.behaviourMap.containsKey("hostile")){
							this.startBehaviour("hostile");
						}
					}
				}
				else{
					if(this.hostileFactions.contains("player")){
						this.hostileTo = glt.play;
						this.seesHostileChar = true;
						if(this.behaviourMap.containsKey("hostile")){
							this.startBehaviour("hostile");
						}	
					}
				}
			}
		}
		
		HashMap<String,Point> soundsHeard = SoundSystem.getInstance().getSoundsAtPoint(this.position, 0);
		if(soundsHeard.containsKey("gunshot")){
			if(this.behaviourMap.containsKey("flee")){
				if(this.currentBehaviour.name.compareTo("flee") != 0){
					this.startBehaviour("flee");
					this.fleeingFrom = soundsHeard.get("gunshot");
				}
			}
		
		}
		
		this.calculateActionsFromBehaviours(p,rot);
	}
	
	public double calculateRotationFromMouse(Point mousePos, Point centre){
		double rot = 0;
		int relativeX = mousePos.x - centre.x;
		int relativeY = centre.y - mousePos.y;
		//System.out.println("cX" + centre.x + "mY"+ centre.y);
		//System.out.println("rel X" + relativeX + "relY"+ relativeY);
		//Q1
		if(relativeX > 0 && relativeY > 0){
			double angle = Math.atan((double)relativeX/(double)relativeY);
			angle = Math.toDegrees(angle);
			rot = angle;
		}
		//Q2
		if(relativeX > 0 && relativeY < 0){
			double angle = Math.atan((double)-relativeY/(double)relativeX);
			angle = Math.toDegrees(angle);
			rot = angle + 90;
		}
		//Q3
		if(relativeX < 0 && relativeY < 0){
			double angle = Math.atan((double)-relativeX/(double)-relativeY);
			angle = Math.toDegrees(angle);
			rot = angle + 180;
		}
		//Q4
		if(relativeX < 0 && relativeY > 0){
			double angle = Math.atan((double)relativeY/(double)-relativeX);
			angle = Math.toDegrees(angle);
			rot = angle + 270;
		}
		if(relativeY == 0 && relativeX >= 0){
			return 90;
		}
		if(relativeY == 0 && relativeX < 0){
			return 270;
		}
		if(relativeX == 0 && relativeY >= 0){
			return 180;
		}
		if(relativeX == 0 && relativeY < 0){
			return 0;
		}
		return rot;
		
	}
	
}
