import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Dimension2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.time.Clock;
import java.util.ArrayList;
import java.util.Stack;

import org.json.simple.*;
import org.json.simple.parser.JSONParser;

public class GameLoopTest extends JFrame implements ActionListener, KeyListener, MouseListener {

	static GameLoopTest instance;
	
	static int GAMEPANELWIDTH = 1000;
	static int GAMEPANELHEIGHT = 700;

	protected JTextField textField;
	protected JTextArea textArea;

	private GamePanel gamePanel = new GamePanel();
	private JButton startButton = new JButton("Start");
	private JButton quitButton = new JButton("Quit");
	private boolean running = false;
	private boolean paused = false;
	private boolean loaded = false;
	private boolean inved = false;


	Clock gameClock = Clock.systemDefaultZone();
	
	long elapsedTime = 0;
	long tick = 0;
	long tock = 0;
	
	Entity dialogueScreen;
	Entity playerHead;
	Entity partnerHead;
	int EXITBUTTONX = 750;
	int EXITBUTTONY = 450;
	Entity exitButton = new Entity(EXITBUTTONX, EXITBUTTONY, "exit", "Exit.png");

	int SCREENX = 0;
	int SCREENY = 0;
	
	Entity inventoryScreen;
	
	int gunHoleX = 715;
	int gunHoleY = 459;
	Entity gunHole = new Entity(gunHoleX, gunHoleY, "gunHole", "gunHole.png");

	Entity reloadIcon = new Entity(gunHoleX - 63, gunHoleY, "reload", "reloadIcon.png");

	int dialoguePartnerIndex;
	DialogueTree currentDialogueTree;
	Statement currentStatement;

	int[] xs = { 0, 2472, 2472, 0 };
	int[] ys = { 0, 0, 1708, 1708 };
	Area currentArea = new Area(xs, ys, "level1");

	Camera cam = new Camera(GAMEPANELWIDTH, GAMEPANELHEIGHT);

	ScreenMode currentMode = ScreenMode.STANDARD;

	static Player play;
	private int buttonClicked = 0;
	private int buttonUnClicked = 0;
	private int fps = 60;
	private int frameCount = 0;

	boolean clicked = false;
	boolean rightClicked = false;
	
	boolean leftPressed = false;
	boolean leftReleased = false;

	int GameState = 0;

	private boolean played = false;

	private KeybMouse keyb;

	Point mousePos = new Point();

	BufferedImage funt;
	static ArrayList<Entity> entityList = new ArrayList<Entity>();
	static ArrayList<Character> charList = new ArrayList<Character>();
	static ArrayList<Trans> transList = new ArrayList<Trans>();
	static ArrayList<Projectile> projList = new ArrayList<Projectile>();
	static ArrayList<GroundItem> groundItemsList = new ArrayList<GroundItem>();
	static ArrayList<Explosion> explosionList = new ArrayList<Explosion>();
	static ArrayList<Door> doorList = new ArrayList<Door>();
	
	ArrayList<DialogueText> dialogueList = new ArrayList<DialogueText>();
	
	ArrayList<HUDElement> itemClickHUDList = new ArrayList<HUDElement>();
	
	ArrayList<InvScreenItem> invScreenItemsList = new ArrayList<InvScreenItem>();
	Entity equippedItemInv = new Entity(0,0,"equipped","defaultIcon.png");
	Entity equippedArmourInv = new Entity(0,0,"equippedArmour","defaultIcon.png");

	ArrayList<Event> eventsList = new ArrayList<Event>();
	ArrayList<Trigger> locTriggerList = new ArrayList<Trigger>();
	ArrayList<Trigger> convoTriggerList = new ArrayList<Trigger>();
	
	JSONArray armourPresets = new JSONArray();
	Graph nodeGraph = new Graph();

	public GameLoopTest() {

		super("Fixed Timestep Game Loop Test");

		// dTest = loadDialogue("dTest.txt");

		/*
		nodeGraph.addNode(0, 0);
		nodeGraph.addNode(10, 10);
		nodeGraph.addNode(20, 20);
		nodeGraph.addNode(30, 30);
		nodeGraph.addNode(40, 40);

		nodeGraph.addNode(30, 20);
		nodeGraph.addNode(10, 0);
		nodeGraph.addNode(30, 0);

		nodeGraph.addEdge("Node(0,0)", "Node(10,10)");
		nodeGraph.addEdge("Node(0,0)", "Node(10,0)");
		
		nodeGraph.addEdge("Node(10,0)", "Node(30,0)");
		
		nodeGraph.addEdge("Node(10,10)", "Node(20,20)");
		
		nodeGraph.addEdge("Node(20,20)", "Node(30,30)");
		
		nodeGraph.addEdge("Node(30,0)", "Node(30,20)");

		nodeGraph.addEdge("Node(30,20)", "Node(30,30)");

		nodeGraph.addEdge("Node(30,30)", "Node(40,40)");

		Stack<Node> path = PathFinder.findPath(nodeGraph, nodeGraph.nodes.get("Node(0,0)"), nodeGraph.nodes.get("Node(30,30)"));
		
		Node move = path.pop();
		*/
		Container cp = getContentPane();
		cp.setLayout(new BorderLayout());
		JPanel p = new JPanel();
		// p.setLayout(new GridLayout(1, 2));
		textField = new JTextField(20);
		textField.addActionListener(this);

		textArea = new JTextArea(5, 20);
		textArea.setEditable(false);
		JScrollPane scrollPane = new JScrollPane(textArea);

		p.add(textField);
		p.add(scrollPane);
		p.add(startButton);
		p.add(quitButton);
		p.setBounds(0, 650, 1000, 50);

		cp.add(gamePanel, BorderLayout.CENTER);
		cp.add(p, BorderLayout.SOUTH);
		setSize(GAMEPANELWIDTH, GAMEPANELHEIGHT);
		this.keyb = new KeybMouse();

		playerHead = new Entity(0, 0, "playerHead", "playerHead.png");
		partnerHead = new Entity(0, 0, "partnerHead", "partnerHead.png");
		dialogueScreen = new Entity(0, 0, "dialogueScreen", "dialogue.png");

		inventoryScreen = new Entity(0,0,"inventoryScreen","inv.png");
		
		this.addKeyListener(this);
		this.addMouseListener(this);
		startButton.addActionListener(this);
		quitButton.addActionListener(this);
	}

	public static void main(String[] args) {
		instance = new GameLoopTest();
		instance.setVisible(true);
	}
	
	public static GameLoopTest getInstance(){
		return instance;
	}

	public void actionPerformed(ActionEvent e) {
		Object s = e.getSource();
		if (s == textField) {
			String text = textField.getText();
			textArea.append(text + "\n");
			textField.selectAll();
			System.out.println("FUUUUNT");
			// Make sure the new text is visible, even if there
			// was a selection in the text area.
			textArea.setCaretPosition(textArea.getDocument().getLength());
			this.gamePanel.requestFocus();
		}

		if (s == startButton) {
			running = !running;
			if (running) {
				startButton.setText("Stop");
				if (loaded == false) {
					loadEvents("events.txt");
					loadLevel("Level1");
					loaded = true;
				}
				runGameLoop();
				this.requestFocus();
				GameState = 1;
			} else {
				startButton.setText("Start");
			}
		} else if (s == quitButton) {
			System.exit(0);
		}
	}

	// Starts a new thread and runs the game loop in it.
	public void runGameLoop() {
		Thread loop = new Thread() {
			public void run() {
				gameLoop();
			}
		};
		loop.start();
	}

	// Only run this in another Thread!
	private void gameLoop() {
		// This value would probably be stored elsewhere.
		final double GAME_HERTZ = 30.0;
		// Calculate how many ns each frame should take for our target game
		// hertz.
		final double TIME_BETWEEN_UPDATES = 1000000000 / GAME_HERTZ;
		// At the very most we will update the game this many times before a new
		// render.
		// If you're worried about visual hitches more than perfect timing, set
		// this to 1.
		final int MAX_UPDATES_BEFORE_RENDER = 5;
		// We will need the last update time.
		double lastUpdateTime = System.nanoTime();
		// Store the last time we rendered.
		double lastRenderTime = System.nanoTime();

		// If we are able to get as high as this FPS, don't render again.
		final double TARGET_FPS = 60;
		final double TARGET_TIME_BETWEEN_RENDERS = 1000000000 / TARGET_FPS;

		// Simple way of finding FPS.
		int lastSecondTime = (int) (lastUpdateTime / 1000000000);

		while (running) {
			double now = System.nanoTime();
			int updateCount = 0;

			if (!paused) {
				// Do as many game updates as we need to, potentially playing
				// catchup.
				while (now - lastUpdateTime > TIME_BETWEEN_UPDATES && updateCount < MAX_UPDATES_BEFORE_RENDER) {
					updateGame();
					lastUpdateTime += TIME_BETWEEN_UPDATES;
					updateCount++;
				}

				// If for some reason an update takes forever, we don't want to
				// do an insane number of catchups.
				// If you were doing some sort of game that needed to keep EXACT
				// time, you would get rid of this.
				if (now - lastUpdateTime > TIME_BETWEEN_UPDATES) {
					lastUpdateTime = now - TIME_BETWEEN_UPDATES;
				}

				// Render. To do so, we need to calculate interpolation for a
				// smooth render.
				float interpolation = Math.min(1.0f, (float) ((now - lastUpdateTime) / TIME_BETWEEN_UPDATES));
				drawGame(interpolation);
				lastRenderTime = now;

				// Update the frames we got.
				int thisSecond = (int) (lastUpdateTime / 1000000000);
				if (thisSecond > lastSecondTime) {
					System.out.println("NEW SECOND " + thisSecond + " " + frameCount);
					fps = frameCount;
					frameCount = 0;
					lastSecondTime = thisSecond;
				}

				// Yield until it has been at least the target time between
				// renders. This saves the CPU from hogging.
				while (now - lastRenderTime < TARGET_TIME_BETWEEN_RENDERS
						&& now - lastUpdateTime < TIME_BETWEEN_UPDATES) {
					Thread.yield();

					// This stops the app from consuming all your CPU. It makes
					// this slightly less accurate, but is worth it.
					// You can remove this line and it will still work (better),
					// your CPU just climbs on certain OSes.
					// FYI on some OS's this can cause pretty bad stuttering.
					// Scroll down and have a look at different peoples'
					// solutions to this.
					try {
						Thread.sleep(1);
					} catch (Exception e) {
					}

					now = System.nanoTime();
				}
			}
		}
	}

	private void updateGame() {
		this.SCREENX = this.getX();
		this.SCREENY = this.getY();

		gamePanel.update();
		
	}

	private void drawGame(float interpolation) {
		gamePanel.setInterpolation(interpolation);
		gamePanel.repaint();
	}

	class GamePanel extends JPanel {
		float interpolation;

		int lastDrawX, lastDrawY;
		Entity e;

		public GamePanel() {

		}

		public void setInterpolation(float interp) {
			interpolation = interp;
		}

		public void update() {
			mousePos = MouseInfo.getPointerInfo().getLocation();
			mousePos.x -= SCREENX;
			mousePos.y -= SCREENY + 25;
			System.out.println("SCREEN y" + SCREENY);
			keyb.xCoord = mousePos.x + cam.x;
			keyb.yCoord = mousePos.y + cam.y;
			if(currentMode == ScreenMode.ITEMPICKUP){
				for(HUDElement he: itemClickHUDList){
					he.update(keyb);
					
					if(he.clickState == 3){
							if(he.name.equals("pickup")){
								int ind = he.itemIndex;
								play.inv.contents.add(groundItemsList.get(ind).getItem());
								groundItemsList.remove(groundItemsList.get(ind));
								itemClickHUDList = new ArrayList<HUDElement>();
								currentMode = ScreenMode.STANDARD;
							}
							if(he.name.equals("inspect")){
								itemClickHUDList = new ArrayList<HUDElement>();
								currentMode = ScreenMode.STANDARD;
							}
							if(he.name.equals("open door")){
								itemClickHUDList = new ArrayList<HUDElement>();
								if(he.listID.compareTo("doorList") == 0){
									doorList.get(he.itemIndex).open();
								}
								currentMode = ScreenMode.STANDARD;
							}
							if(he.name.equals("close door")){
								itemClickHUDList = new ArrayList<HUDElement>();
								if(he.listID.compareTo("doorList") == 0){
									doorList.get(he.itemIndex).close();
								}
								currentMode = ScreenMode.STANDARD;
							}
							if(he.name.equals("cancel")){
								itemClickHUDList = new ArrayList<HUDElement>();
								currentMode = ScreenMode.STANDARD;
							}
					}
				}
				if(keyb.space == true){
					itemClickHUDList = new ArrayList<HUDElement>();
					currentMode = ScreenMode.STANDARD;
					keyb.space = false;
				}
				
			}
			
			if (currentMode == ScreenMode.STANDARD|| currentMode == ScreenMode.DMOUSE) {

				checkTriggers();
				
				if (play != null) {
					play.update(keyb, mousePos);
					cam.updateCam(play, currentArea);
				}
				boolean skip = false;
				//HUD ELEMENTS
				if(clicked == true){					
					if(reloadIcon.isClick(mousePos)){
						play.reload();
						skip = true;
					}
				}
				SoundSystem.getInstance().update();
				ArrayList<Character> cCopy = (ArrayList<Character>) charList.clone();
				for (Character c : charList) {
					// check click
					if (clicked == true) {
						int mousePosx = mousePos.x + cam.x;
						int mousePosy = mousePos.y + cam.y;
						c.checkClick(new Point(mousePosx, mousePosy));
						skip = true;
					}
					c.update(keyb);
					if (c.health <= 0) {
						cCopy.remove(c);
						continue;
					}
				}
				if(!skip){
				for(GroundItem i: groundItemsList){
					i.update(keyb);
					if(i.clickState == 3){
							//int mousePosx = mousePos.x + cam.x;
							//int mousePosy = mousePos.y + cam.y;
							int ind = groundItemsList.indexOf(i);
							makeEntityClickHUD(new Point(keyb.xCoord, keyb.yCoord),i,"groundItemsList",ind);
							currentMode = ScreenMode.ITEMPICKUP;
							break;
						}
				}
				}
				charList = cCopy;
				
				ArrayList<Door> dCopy = (ArrayList<Door>) doorList.clone();
				if(!skip){
				for (Door d : doorList) {
					d.update(keyb);
					play.collisionCheck(d);
					if(d.clickState == 3){
						int ind = doorList.indexOf(d);
						makeEntityClickHUD(new Point(keyb.xCoord, keyb.yCoord),d,"doorList",ind);
						currentMode = ScreenMode.ITEMPICKUP;
						break;
					}
					if(d.health <= 0){
						dCopy.remove(d);
						continue;
					}
				}
				doorList = dCopy;
				}
				if(false){
				for (Entity e : entityList) {
					e.update(keyb);
					if(e.clickState == 3){
						//int mousePosx = mousePos.x + cam.x;
						//int mousePosy = mousePos.y + cam.y;
						int ind = entityList.indexOf(e);
						makeEntityClickHUD(new Point(keyb.xCoord, keyb.yCoord),e,"entityList",ind);
						currentMode = ScreenMode.ITEMPICKUP;
						break;
					}
				}
				}
				for (Trans t : transList) {
					// check click
					if (clicked == true) {
						int mousePosx = mousePos.x + cam.x;
						int mousePosy = mousePos.y + cam.y;
						Command c = t.checkClick(new Point(mousePosx, mousePosy));
						if (c.text != "null") {
							entityList = new ArrayList<Entity>();
							charList = new ArrayList<Character>();
							transList = new ArrayList<Trans>();
							loadLevel(c.text);
							break;
						}
					}
					t.update(keyb);
				}
				
				ArrayList<Projectile> pCopy = (ArrayList<Projectile>) projList.clone();
				for (Projectile p : projList) {
					p.update(entityList, charList, doorList);
					p.checkTimer();
					if (p.hit) {
						String hitComm = p.getHitCommand();
						processGameCommand(hitComm);
						pCopy.remove(p);
					}
				}
				projList = pCopy;
				
				ArrayList<Explosion> eCopy = (ArrayList<Explosion>) explosionList.clone();
				for (Explosion e : explosionList) {
					e.update(entityList, charList,doorList);
					e.checkTimer();
					if (e.dead) {
						eCopy.remove(e);
					}
				}
				explosionList = eCopy;

				//COLLISIONS
				//FOR NOW WE ARE JUST DOING PLAYER COLLISIONS,AND CHARACTER COLLISIONS
				
				//PLayer
				for(Entity e: entityList){
					if(e.collideable == true){
						play.collisionCheck(e);
						for(Character c: charList){
							c.collisionCheck(e);
							c.rayCollisionCheck();
						}
					}
				}
				play.rayCollisionCheck();
				
			}
			if (currentMode == ScreenMode.DIALOGUE){
				this.setCursor(new Cursor(0));
				if (clicked) {
					mousePos.x += cam.x;
					mousePos.y += cam.y;
					
					if(exitButton.isClick(mousePos)){
						currentMode = ScreenMode.STANDARD;
						dialogueList = new ArrayList<DialogueText>();
					}
					mousePos.x -= cam.x;
					mousePos.y -= cam.y;
					ArrayList<DialogueText> lDl = dialogueList;
					for(int i = 1; i < dialogueList.size();i++){
						if(dialogueList.get(i).isClick(mousePos)){
							ArrayList<String> triggersToTrig = dialogueList.get(i).getEvents();
							for(String e: triggersToTrig){
								for(Trigger t : convoTriggerList){
									if(t.name.equals(e)){
										t = trig(t);
									}
								}
							}
							
							currentStatement = currentDialogueTree.getStatement(i);
							dialogueList = new ArrayList<DialogueText>();
							
							DialogueText d1 = new DialogueText(0,0,"s");
							d1.setText(currentStatement.text);
							d1.screenOrder = 0;
							d1.triggerNames = currentStatement.triggers;
							dialogueList.add(d1);
							int SO = 1;
							for(Response r: currentStatement.responses){
								DialogueText dr = new DialogueText(0,0,"r");
								dr.setText(r.text);
								dr.screenOrder = SO;
								dr.setEvents(r.triggers);
								dialogueList.add(dr);
								SO++;
							}
							
						}
					}
					
				}
					
			}

			if (currentMode == ScreenMode.ATTACK) {
				if (clicked) {
					mousePos.x += cam.x;
					mousePos.y += cam.y;
					processGameCommand(play.useEquippedItem(new Point(keyb.xCoord + SCREENX*2,keyb.yCoord + SCREENY + 25)).text);
					currentMode = ScreenMode.STANDARD;
				}
			}
			if(currentMode == ScreenMode.DMOUSE){
				if (clicked) {
					mousePos.x += cam.x;
					mousePos.y += cam.y;
					for(Character c : charList){
						if(mousePos.distance(new Point((int)c.xPos,(int)c.yPos)) < c.halfX){
							processGameCommand(c.dialogueClick().text);
							break;
						}
					}
				}
			}
			if (currentMode == ScreenMode.INVENTORY) {
				keyb.xCoord = mousePos.x;
				keyb.yCoord = mousePos.y;
				for(int j = 0; j < invScreenItemsList.size();j++){
					invScreenItemsList.get(j).update(keyb);
					if(invScreenItemsList.get(j).clickState == 3){
						int ind = j;
						makeEntityClickHUD(new Point(keyb.xCoord, keyb.yCoord),invScreenItemsList.get(j),"invScreenItemsList",ind);
						currentMode = ScreenMode.INVENTORYITEMCLICKED;
						break;
					}
				}
				equippedItemInv.update(keyb);
				if(equippedItemInv.clickState == 3){
					makeEntityClickHUD(new Point(keyb.xCoord, keyb.yCoord),equippedItemInv,"equippedItem",-1);
					currentMode = ScreenMode.INVENTORYITEMCLICKED;
				}
				equippedArmourInv.update(keyb);
				if(equippedArmourInv.clickState == 3){
					makeEntityClickHUD(new Point(keyb.xCoord, keyb.yCoord),equippedArmourInv,"equippedArmour",-1);
					currentMode = ScreenMode.INVENTORYITEMCLICKED;
				}

			}
			if (currentMode == ScreenMode.INVENTORYITEMCLICKED) {
				keyb.xCoord = mousePos.x;
				keyb.yCoord = mousePos.y;
				for(HUDElement he: itemClickHUDList){
					he.update(keyb);
					
					if(he.clickState == 3){
							if(he.name.equals("unequip")){
								if(he.listID.compareTo("equippedItem")==0){
									play.inv.contents.add(play.equippedItem);
									play.equippedItem = null;
									equippedItemInv = new Entity(0,0,"equipped","defaultIcon.png");
									{
										inventoryScreen.scaleToScreen(this.getWidth(), this.getHeight());
										inventoryScreen.xPos = cam.x;
										inventoryScreen.yPos = cam.y;
										currentMode = ScreenMode.INVENTORY;
										itemClickHUDList = new ArrayList<HUDElement>();
										if(play.equippedItem != null){
											equippedItemInv.updateTexture(play.equippedItem.iconFilename);
										}
										equippedItemInv.xPos = cam.x + (this.getWidth() *.7);
										equippedItemInv.yPos = cam.y + (this.getHeight() *.6);
										
										if(play.equippedArmour != null){
											equippedArmourInv.updateTexture(play.equippedArmour.iconFilename);
										}
										equippedArmourInv.xPos = cam.x + (this.getWidth() *.7 );
										equippedArmourInv.yPos = cam.y + (this.getHeight() *.6 - 200);
										
										int SO = 1;
										invScreenItemsList = new ArrayList<InvScreenItem>();
										for(Item b:play.inv.contents){
											if(b == null)continue;
											InvScreenItem ie = new InvScreenItem(0,0,b.name);
											ie.update(keyb);
											ie.setText(b.name);
											ie.screenOrder = SO;
											SO ++;
											invScreenItemsList.add(ie);
										}
									}
									currentMode = ScreenMode.INVENTORY;
								}
								if(he.listID.compareTo("equippedArmour")==0){
									play.inv.contents.add(play.equippedArmour);
									play.equippedArmour = null;
									equippedArmourInv = new Entity(0,0,"equippedArmour","defaultIcon.png");
									{
										inventoryScreen.scaleToScreen(this.getWidth(), this.getHeight());
										inventoryScreen.xPos = cam.x;
										inventoryScreen.yPos = cam.y;
										itemClickHUDList = new ArrayList<HUDElement>();
										currentMode = ScreenMode.INVENTORY;
										if(play.equippedItem != null){
											equippedItemInv.updateTexture(play.equippedItem.iconFilename);
										}
										equippedItemInv.xPos = cam.x + (this.getWidth() *.7);
										equippedItemInv.yPos = cam.y + (this.getHeight() *.6);
										
										if(play.equippedArmour != null){
											equippedArmourInv.updateTexture(play.equippedArmour.iconFilename);
										}
										equippedArmourInv.xPos = cam.x + (this.getWidth() *.7 );
										equippedArmourInv.yPos = cam.y + (this.getHeight() *.6 - 200);
										
										int SO = 1;
										invScreenItemsList = new ArrayList<InvScreenItem>();
										for(Item b:play.inv.contents){
											if(b == null)continue;
											InvScreenItem ie = new InvScreenItem(0,0,b.name);
											ie.update(keyb);
											ie.setText(b.name);
											ie.screenOrder = SO;
											SO ++;
											invScreenItemsList.add(ie);
										}
									}
									currentMode = ScreenMode.INVENTORY;
								}
							}
							if(he.name.equals("equip")){
								itemClickHUDList = new ArrayList<HUDElement>();
								int ind = he.itemIndex;
								String invName = invScreenItemsList.get(ind).name;
								ArrayList<Item> localInv = play.inv.contents;
								for(int i = 0; i< play.inv.contents.size();i++){
									if(play.inv.contents.get(i).name == invName){
										if(play.inv.contents.get(i) instanceof Armour){
											if(play.equippedArmour != null){
												play.inv.contents.add(play.equippedArmour);
											}
											play.equippedArmour = (Armour)play.inv.contents.get(i);
											play.inv.contents.remove(play.equippedArmour);
											break;
										}
										else{
											if(play.equippedItem != null){
												play.inv.contents.add(play.equippedItem);
											}
											play.equippedItem = play.inv.contents.get(i);
											play.inv.contents.remove(play.equippedItem);
											break;	
										}
										
									}
								}
								{
									inventoryScreen.scaleToScreen(this.getWidth(), this.getHeight());
									inventoryScreen.xPos = cam.x;
									inventoryScreen.yPos = cam.y;
									currentMode = ScreenMode.INVENTORY;
									if(play.equippedItem != null){
										equippedItemInv.updateTexture(play.equippedItem.iconFilename);
									}
									equippedItemInv.xPos = cam.x + (this.getWidth() *.7);
									equippedItemInv.yPos = cam.y + (this.getHeight() *.6);
									
									if(play.equippedArmour != null){
										equippedArmourInv.updateTexture(play.equippedArmour.iconFilename);
									}
									equippedArmourInv.xPos = cam.x + (this.getWidth() *.7 );
									equippedArmourInv.yPos = cam.y + (this.getHeight() *.6 - 200);
									
									int SO = 1;
									invScreenItemsList = new ArrayList<InvScreenItem>();
									for(Item b:play.inv.contents){
										if(b == null)continue;
										InvScreenItem ie = new InvScreenItem(0,0,b.name);
										ie.update(keyb);
										ie.setText(b.name);
										ie.screenOrder = SO;
										SO ++;
										invScreenItemsList.add(ie);
									}
								}
								currentMode = ScreenMode.INVENTORY;

							}
							if(he.name.equals("drop")){
								itemClickHUDList = new ArrayList<HUDElement>();
								currentMode = ScreenMode.INVENTORY;
								int ind = he.itemIndex;
								processGameCommand("dropItem>player>" + invScreenItemsList.get(ind).name +">" + (int)(play.xPos) + ">" + (int)(play.yPos));
								{
									inventoryScreen.scaleToScreen(this.getWidth(), this.getHeight());
									inventoryScreen.xPos = cam.x;
									inventoryScreen.yPos = cam.y;
									currentMode = ScreenMode.INVENTORY;
									if(play.equippedItem != null){
										equippedItemInv.updateTexture(play.equippedItem.iconFilename);
									}
									equippedItemInv.xPos = cam.x + (this.getWidth() *.7);
									equippedItemInv.yPos = cam.y + (this.getHeight() *.6);
									
									if(play.equippedArmour != null){
										equippedArmourInv.updateTexture(play.equippedArmour.iconFilename);
									}
									equippedArmourInv.xPos = cam.x + (this.getWidth() *.7 );
									equippedArmourInv.yPos = cam.y + (this.getHeight() *.6 - 200);
									
									int SO = 1;
									invScreenItemsList = new ArrayList<InvScreenItem>();
									for(Item b:play.inv.contents){
										if(b == null)continue;
										InvScreenItem ie = new InvScreenItem(0,0,b.name);
										ie.update(keyb);
										ie.setText(b.name);
										ie.screenOrder = SO;
										SO ++;
										invScreenItemsList.add(ie);
									}
								}
								currentMode = ScreenMode.INVENTORY;
							}
							if(he.name.equals("cancel")){
								itemClickHUDList = new ArrayList<HUDElement>();
								currentMode = ScreenMode.INVENTORY;
							}
					}
				}
				if(rightClicked){
					itemClickHUDList = new ArrayList<HUDElement>();
					currentMode = ScreenMode.INVENTORY;
					keyb.space = false;
				}

			}
			
			clicked = false;
			rightClicked = false;
			if(tick!= 0){
				tock = gameClock.millis();
				long tickTime = tock-tick;
				if (currentMode == ScreenMode.STANDARD ||currentMode == ScreenMode.DMOUSE ) {
					elapsedTime = elapsedTime + tickTime;
					GameClock.getInstance().currentTime = elapsedTime;
				}
			}
			tick = gameClock.millis();
			
		}

		public void checkTriggers() {

			for (Trigger t : locTriggerList) {
				System.out.println(t.location.distance(play.centre));
				if (t.level.compareTo(currentArea.name) != 0) {
					System.out.println(currentArea.name + t.level);
					continue;
				}
				if (t.location.distance(play.centre) < 20) {
					t = trig(t);
				}
			}

			// REMOVE OUTTRIGGED TRIGS
			ArrayList<Trigger> tCopy = (ArrayList<Trigger>) locTriggerList.clone();
			for (Trigger t : locTriggerList) {
				if (t.isDead()) {
					tCopy.remove(t);
				}
			}
			locTriggerList = tCopy;
			ArrayList<Trigger> tcCopy = (ArrayList<Trigger>) convoTriggerList.clone();
			for(Trigger t: convoTriggerList){
				if(t.isDead()){
					tCopy.remove(t);
				}
			}
			convoTriggerList = tcCopy;
			
		}

		@SuppressWarnings("deprecation")
		public void paintComponent(Graphics g) {
			// BS way of clearing out the old rectangle to save CPU.
			if (currentMode == ScreenMode.STANDARD || currentMode == ScreenMode.ATTACK|| currentMode == ScreenMode.DMOUSE || currentMode == ScreenMode.ITEMPICKUP) {
				g.setColor(getBackground());
				g.fillRect(0, 0, 10000, 10000);
				for (Entity e : entityList) {
					e.draw(g, this, cam);
				}
				for (Door d : doorList) {
					d.draw(g, this, cam);
				}
				
				for (Projectile p : projList) {
					p.draw(g, this, cam);
				}
				for (Trans t : transList) {
					t.draw(g, this, cam);
				}
				for(GroundItem i: groundItemsList){
					i.draw(g,this,cam);
				}
				for (Character c : charList) {
					c.draw(g, this, cam);
				}
				if (play != null) {
					play.draw(g, this, cam);
				}
				for(Explosion ex: explosionList){
					ex.draw(g,this,cam);
				}
				for(HUDElement he: itemClickHUDList){
					he.draw(g, this, cam);
				}
				/*
				for (String s: nodeGraph.nodes.keySet()) {
					g.drawOval(nodeGraph.nodes.get(s).xPos, nodeGraph.nodes.get(s).yPos, 10, 10);
				}*/
				Camera hudC = new Camera(0,0);
				//HUD ELEMENTS GO AFTER HERE
				gunHole.draw(g, this,hudC);
				reloadIcon.draw(g,this , hudC);
				if(play == null )return;
				if(play.equippedItem != null){
					if(play.equippedItem instanceof Weapon){
						Entity gunIcon = new Entity((int)gunHole.xPos + 9, (int)gunHole.yPos + 25, "gunIcon", play.equippedItem.iconFilename);
						gunIcon.draw(g, this, hudC);
						Weapon tempW = (Weapon)play.equippedItem;
						g.drawString("Ammo: " + tempW.loadedAmmunition + " / " +tempW.clipSize, (int)gunHole.xPos + 9, (int)gunHole.yPos + 25);						
					}
				}

			}
			
			if(currentMode == ScreenMode.DMOUSE){
				Cursor cu = new Cursor(7);
				this.setCursor(cu);
			}
			if (currentMode == ScreenMode.DIALOGUE){
				dialogueScreen.draw(g,this,cam);
				playerHead.draw(g,this,cam);
				partnerHead.draw(g,this,cam);
				for (DialogueText d: dialogueList) {
					d.draw(g, this, cam);
				}
				exitButton.draw(g, this, cam);
				
			}
			if(currentMode == ScreenMode.INVENTORY ||currentMode == ScreenMode.INVENTORYITEMCLICKED){
				inventoryScreen.draw(g, this, cam);
				equippedItemInv.draw(g, this, cam);
				equippedArmourInv.draw(g, this, cam);
				for (InvScreenItem i: invScreenItemsList) {
					i.draw(g, this, cam);
				}
				for(HUDElement he: itemClickHUDList){
					he.draw(g, this, new Camera(0,0));
				}
			}
			if (currentMode == ScreenMode.ATTACK) {
				Cursor cu = new Cursor(1);
				this.setCursor(cu);
			}
			if (currentMode == ScreenMode.STANDARD) {
				Cursor cu = new Cursor(0);
				this.setCursor(cu);
			}
			g.setColor(Color.BLACK);
			g.drawString("FPS: " + fps, 5, 10);
			
			
			frameCount++;
			
			g.setColor(Color.BLUE);
			g.drawString("TIME: " + elapsedTime, 10, 20);
		}

		
		public Trigger  trig(Trigger t){
			for(Event e: t.targetEvents){
				processGameCommand(e.command);
			}
			t.decrement();
			return t;
		}

	}

	@Override
	public void keyPressed(KeyEvent arg0) {
		buttonClicked = arg0.getKeyCode();
		if (buttonClicked == 37) {
			keyb.left = true;
		}
		if (buttonClicked == 39) {
			keyb.right = true;
		}
		if (buttonClicked == 38) {
			keyb.down = true;
		}
		if (buttonClicked == 40) {
			keyb.up = true;
		}
		if (buttonClicked == 87) {
			keyb.w = true;
		}
		if (buttonClicked == 83) {
			keyb.s = true;
		}
		if (buttonClicked == 68) {
			keyb.d = true;
		}
		if (buttonClicked == 65) {
			keyb.a = true;
		}
		if (buttonUnClicked == 32) {
			keyb.space = true;
		}

		if (buttonClicked == 16) {
			if (currentMode == ScreenMode.STANDARD) {
				currentMode = ScreenMode.ATTACK;
			} else {
				currentMode = ScreenMode.STANDARD;
			}
		}
		
		if (buttonClicked == 84) {
			if (currentMode == ScreenMode.STANDARD) {
				currentMode = ScreenMode.DMOUSE;
			} else if(currentMode == ScreenMode.DMOUSE){
				currentMode = ScreenMode.STANDARD;
			}
		}
		
		if (buttonClicked == 73) {
			if (currentMode == ScreenMode.STANDARD) {
				inventoryScreen.scaleToScreen(this.getWidth(), this.getHeight());
				inventoryScreen.xPos = cam.x;
				inventoryScreen.yPos = cam.y;
				currentMode = ScreenMode.INVENTORY;
				if(play.equippedItem != null){
					equippedItemInv.updateTexture(play.equippedItem.iconFilename);
				}
				equippedItemInv.xPos = cam.x + (this.getWidth() *.7);
				equippedItemInv.yPos = cam.y + (this.getHeight() *.6);
				
				if(play.equippedArmour != null){
					equippedArmourInv.updateTexture(play.equippedArmour.iconFilename);
				}
				equippedArmourInv.xPos = cam.x + (this.getWidth() *.7);
				equippedArmourInv.yPos = cam.y + (this.getHeight() *.6 - 200);
				int SO = 1;
				for(Item i:play.inv.contents){
					InvScreenItem ie = new InvScreenItem(0,0,i.name);
					ie.update(keyb);
					ie.setText(i.name);
					ie.screenOrder = SO;
					SO ++;
					invScreenItemsList.add(ie);
				}
			} else {
				invScreenItemsList = new ArrayList<InvScreenItem>();
				currentMode = ScreenMode.STANDARD;
			}
		}
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		buttonUnClicked = arg0.getKeyCode();
		if (buttonUnClicked == 37) {
			keyb.left = false;
		}
		if (buttonUnClicked == 39) {
			keyb.right = false;
		}
		if (buttonUnClicked == 38) {
			keyb.down = false;
		}
		if (buttonUnClicked == 40) {
			keyb.up = false;
		}
		if (buttonUnClicked == 87) {
			keyb.w = false;
		}
		if (buttonUnClicked == 83) {
			keyb.s = false;
		}
		if (buttonUnClicked == 68) {
			keyb.d = false;
		}
		if (buttonUnClicked == 65) {
			keyb.a = false;
		}
		if (buttonUnClicked == 32) {
			keyb.space = false;
		}
		
	}
	
	
	public void makeEntityClickHUD(Point p,Entity e,String list,int index){
		if(list.compareTo("equippedItem")==0){
			HUDElement HE = new HUDElement(p.x,p.y + 0,"unequip","equippedItem",-1);
			this.itemClickHUDList.add(HE);
			return;
		}	
		if(list.compareTo("equippedArmour")==0){
			HUDElement HE = new HUDElement(p.x,p.y + 0,"unequip","equippedArmour",-1);
			this.itemClickHUDList.add(HE);
			return;
		}
		
		int yOffset = 0;
		for(String button: e.getClickOptions()){
			HUDElement HE = new HUDElement(p.x,p.y + yOffset,button,list,index);
			yOffset += 20;
			this.itemClickHUDList.add(HE);
		}
	}
	
	/*
	public void makeItemClickHUD(Point p,int itemInd){
		HUDElement pickup = new HUDElement(p.x,p.y,"pickup","groundItemsList",itemInd);
		HUDElement inspect = new HUDElement(p.x +5 ,p.y+20,"inspect","groundItemsList"itemInd);
		this.itemClickHUDList.add(pickup);
		this.itemClickHUDList.add(inspect);
	}
	*/
	
	//HHHH
	
	public void processGameCommand(String command) {
		String[] splitComm = command.split("\\>");
		String comm = splitComm[0];
		switch (comm) {
		
		case "dialogue":
			if(splitComm[1].compareTo("start") == 0){
				dialogueList = new ArrayList<DialogueText>();
				String partner = splitComm[2];
				startDialogue(partner);
				dialogueScreen.scaleToScreen(this.getWidth(), this.getHeight());
				dialogueScreen.xPos =  cam.x;
				dialogueScreen.yPos =  cam.y;
				playerHead.scale = dialogueScreen.scale;
				playerHead.xPos = dialogueScreen.scale *80 + cam.x;
				playerHead.yPos = dialogueScreen.scale * 51 + cam.y;
				partnerHead.scale = dialogueScreen.scale;
				partnerHead.xPos = dialogueScreen.scale *935 + cam.x;
				partnerHead.yPos = dialogueScreen.scale * 51 + cam.y;

			}
			if(splitComm[1].compareTo("end") == 0){
				currentMode = ScreenMode.STANDARD;
			}
			
			break;
		case "move":
			String target = splitComm[1];
			String lev = splitComm[2];
			String pozx = splitComm[3];
			String pozy = splitComm[4];
			if (target.compareTo("player") == 0) {
				play.xPos = Integer.parseInt(pozx);
				play.yPos = Integer.parseInt(pozy);
			}
			break;

		case "output":
			String out = splitComm[1];
			textArea.append(out + "\n");
			textField.selectAll();
			textArea.setCaretPosition(textArea.getDocument().getLength());
			break;

		case "fire":
			String proj = splitComm[1];
			double vel = Double.parseDouble(splitComm[2]);
			double ang = Double.parseDouble(splitComm[3]);
			int xp = (int) Double.parseDouble(splitComm[4]);
			int yp = (int) Double.parseDouble(splitComm[5]);
			if (proj.compareTo("Bullet") == 0) {
				Point p1 = new Point(0, 0);
				Point p2 = new Point(0, 5);
				Point p3 = new Point(5, 5);
				Point p4 = new Point(5, 0);
				Point[] points = new Point[4];
				points[0] = p1;
				points[1] = p2;
				points[2] = p3;
				points[3] = p4;

				projList.add(new Bullet(xp + SCREENX, yp + SCREENY, "bullet", points, Color.BLACK, ang, vel));
			}
			if (proj.compareTo("Arrow") == 0) {
				Point p1 = new Point(0, 0);
				Point p2 = new Point(0, 5);
				Point p3 = new Point(5, 5);
				Point p4 = new Point(5, 0);
				Point[] points = new Point[4];
				points[0] = p1;
				points[1] = p2;
				points[2] = p3;
				points[3] = p4;

				projList.add(new Arrow(xp, yp, "arrow", points, Color.BLACK, ang, vel));
			}
			break;

		case "throw":
			proj = splitComm[1];
			vel = Double.parseDouble(splitComm[2]);
			ang = Double.parseDouble(splitComm[3]);
			xp = (int) Double.parseDouble(splitComm[4]);
			yp = (int) Double.parseDouble(splitComm[5]);
			if (proj.compareTo("Grenade") == 0) {
				Point p1 = new Point(0, 0);
				Point p2 = new Point(0, 20);
				Point p3 = new Point(20, 20);
				Point p4 = new Point(20, 0);
				Point[] points = new Point[4];
				points[0] = p1;
				points[1] = p2;
				points[2] = p3;
				points[3] = p4;

				Grenade gra = new Grenade(xp, yp, "grenade", points, Color.BLACK, ang, vel);
				gra.birthDay = GameClock.getInstance().currentTime;
				projList.add(gra);
				
				play.unEquip();
				play.inv.removeItem("grenade");
				
			}
			break;
			
		case "giveItem":
			target = splitComm[1];
			String itemClassName = splitComm[2];
			if (target.compareTo("player") == 0) {
				switch(itemClassName){
				case "gun":
					Gun tG = new Gun("gun");
					play.inv.contents.add(tG);
					break;
				case "grenade":
					GrenadeWeapon gr = new GrenadeWeapon("grenade");
					play.inv.contents.add(gr);
					break;
				case "knife":
					break;
				case "bow":
					Bow bw = new Bow("bow");
					play.inv.contents.add(bw);
					break;
				case "ammo":
					String ammoType = splitComm[3];
					String sAmount = splitComm[4];
					String sCapacity = splitComm[5];
					int amount = Integer.parseInt(sAmount);
					int capacity = Integer.parseInt(sCapacity);
					AmmoBox aB = new AmmoBox("box of "+ammoType,capacity,ammoType);
					aB.contents = amount;
					play.inv.contents.add(aB);
				}
			}
			break;
		case "dropItem":
			target = splitComm[1];
			String itemName = splitComm[2];
			int xDrop = Integer.parseInt(splitComm[3]);
			int yDrop = Integer.parseInt(splitComm[4]);
			
			if (target.compareTo("player") == 0) {
				for (Item i : play.inv.contents) {
					if (i.name.compareTo(itemName) == 0){
						GroundItem gi = new GroundItem(xDrop,yDrop,"itemName","dropItem.png");
						gi.setItem(i);
						groundItemsList.add(gi);
						play.inv.contents.remove(i);
						break;
					}
				}
			}
			break;
		case "equip":
			target = splitComm[1];
			String itemorWep = splitComm[2];
			String item = splitComm[3];
			if (target.compareTo("player") == 0) {
				if(itemorWep.equals("item")){
					for (Item i : play.inv.contents) {
						if (i.name.compareTo(item) == 0) {
							play.Equip(i);
						}
					}
				}
				if(itemorWep.equals("weapon")){
					for (Item i : play.inv.contents) {
						if (i.name.compareTo(item) == 0) {
							play.EquipWeapon((Weapon)i);
						}
					}
				}
			}
			break;
		case "create":
			String thingToCreate = splitComm[1];
			xp = (int) Double.parseDouble(splitComm[2]);
			yp = (int) Double.parseDouble(splitComm[3]);
			if(thingToCreate.compareTo("explosion") == 0){
				int diameter = (int) Double.parseDouble(splitComm[4]);
				Explosion exp = new Explosion(xp,yp,"boom",diameter);
				explosionList.add(exp);
			}
			
		}	
	}
	
	public void startDialogue(String partner){
		for(int i = 0; i < charList.size();i++){
			if(charList.get(i).name.compareTo(partner) == 0){
				this.dialoguePartnerIndex = i;
			}
		}		
		
		if(charList.get(dialoguePartnerIndex).dialogue.statementList.size() == 0){
			return;
		}
		currentDialogueTree = charList.get(dialoguePartnerIndex).dialogue;
		currentStatement = currentDialogueTree.getStatement(0);
		currentDialogueTree.currentStatement = currentDialogueTree.getStatement(0);
		
		DialogueText d1 = new DialogueText(0,0,"s");
		d1.setText(currentStatement.text);
		d1.screenOrder = 0;
		dialogueList.add(d1);
		int SO = 1;
		for(Response r: currentStatement.responses){
			DialogueText dr = new DialogueText(0,0,"r");
			dr.setText(r.text);
			dr.screenOrder = SO;
			dr.setEvents(r.triggers);
			dialogueList.add(dr);
			SO++;
		}
		exitButton.xPos = cam.x + EXITBUTTONX;
		exitButton.yPos = cam.y + EXITBUTTONY;
		currentMode = ScreenMode.DIALOGUE;
		this.setCursor(new Cursor(0));
		
	}
	@Override
	public void keyTyped(KeyEvent arg0) {

	}

	public void loadLevel(String filename){
		currentArea = new Area(xs, ys, filename);
		filename = filename + ".json";
		BufferedImage img = null;
		try {
			img = ImageIO.read(new File("funt.png"));
		} catch (IOException e) {
		}
		funt = img;
		JSONParser parser = new JSONParser();
		try {

            Object obj = parser.parse(new FileReader(filename));

            JSONObject jsonObject = (JSONObject) obj;
            
            //SETUP PLAYER
            JSONObject playerJSON = (JSONObject) jsonObject.get("Player");
            int x = Integer.parseInt(playerJSON.get("x").toString());
            int y = Integer.parseInt(playerJSON.get("y").toString());
            int w = Integer.parseInt(playerJSON.get("w").toString());
            int h = Integer.parseInt(playerJSON.get("h").toString());
            String fn = playerJSON.get("filename").toString();
			Player e = new Player(x, y, "Player",fn,w,h);
			
            if(playerJSON.containsKey("hitbox")){
            	JSONObject hitboxJ = (JSONObject) playerJSON.get("hitbox");
            	String type = hitboxJ.get("type").toString();
            	if(type.compareTo("circle")==0){
    				JSONArray centre  = (JSONArray)hitboxJ.get("centre");
    				int cx = Integer.parseInt(centre.get(0).toString());
    				int cy = Integer.parseInt(centre.get(1).toString());
    	            int diameter = Integer.parseInt(hitboxJ.get("diameter").toString());
    	            
    	            Ellipse2D.Float playerCirc = new Ellipse2D.Float();
    	    		playerCirc.setFrame(cx, cy, diameter,diameter);
    	    		playerCirc.x = (float) cx;
    	    		playerCirc.y = (float) cy;
    	    		e.centre.x = cx + (int)e.xPos;
    	    		e.centre.y = cy + (int)e.yPos;
    	    		e.playerCirc = playerCirc;
            	}
            }
            
			play = e;
			played = true;
			
			//SETUP ENTITIES
			JSONArray entitiesJSON = (JSONArray) jsonObject.get("Entities");
			for(Object EJSON: entitiesJSON){
				JSONObject jo = (JSONObject)EJSON;
				String name  = jo.get("name").toString();
				
				JSONArray pos  = (JSONArray)jo.get("position");
				int posX = Integer.parseInt(pos.get(0).toString());
				int posY = Integer.parseInt(pos.get(1).toString());
				
				JSONArray xVerts  = (JSONArray)jo.get("x");
				JSONArray yVerts  = (JSONArray)jo.get("y");
				Point[] verts = new Point[xVerts.size()];
				for(int i = 0; i < xVerts.size();i++){
					verts[i] = new Point(Integer.parseInt(xVerts.get(i).toString()),Integer.parseInt(yVerts.get(i).toString()));
				}
				boolean collideable = Boolean.parseBoolean(jo.get("collideable").toString());
				boolean moveable = Boolean.parseBoolean(jo.get("moveable").toString());
				boolean text = false;
				if(jo.containsKey("texture")){
					try {
						img = ImageIO.read(new File(jo.get("texture").toString()));
						text = true;
					} catch (IOException ex) {
					}
					funt = img;
				}
				Entity ent;
				if(text){
					ent = new Entity(posX,posY,name,jo.get("texture").toString());
				}else{
					ent = new Entity(posX, posY, name, verts, Color.BLACK,null);					
				}
				
				ent.collideable = collideable;
				ent.moveable = moveable;
				entityList.add(ent);
			}
			//SETUP DOORS
			JSONArray doorsJSON = (JSONArray) jsonObject.get("Doors");
			for(Object DJSON: doorsJSON){
				JSONObject jo = (JSONObject)DJSON;
				String name  = jo.get("name").toString();
				JSONArray pos  = (JSONArray)jo.get("position");
				int posX = Integer.parseInt(pos.get(0).toString());
				int posY = Integer.parseInt(pos.get(1).toString());
				
				int depth = Integer.parseInt(jo.get("depth").toString());
				int width = Integer.parseInt(jo.get("width").toString());
				
				int open = Integer.parseInt(jo.get("openAngle").toString());
				int closed = Integer.parseInt(jo.get("closedAngle").toString());
				
				boolean defaultState = Boolean.parseBoolean(jo.get("default").toString());

				boolean destructible= Boolean.parseBoolean(jo.get("destructible").toString());
				int health = Integer.parseInt(jo.get("health").toString());
				
				Door dore = new Door(posX, posY, name,width,depth,closed,open,health,destructible);
				dore.collideable = true;
				doorList.add(dore);
			}
			
			this.armourPresets = (JSONArray) jsonObject.get("ArmourPresets");

			//SETUP CHARACTERS
			JSONArray charactersJSON = (JSONArray) jsonObject.get("Characters");
			for(Object CJSON: charactersJSON){
				JSONObject jo = (JSONObject)CJSON;
				String name  = jo.get("name").toString();
				
				JSONArray pos  = (JSONArray)jo.get("position");
				int posX = Integer.parseInt(pos.get(0).toString());
				int posY = Integer.parseInt(pos.get(1).toString());
				
				JSONArray xVerts  = (JSONArray)jo.get("x");
				JSONArray yVerts  = (JSONArray)jo.get("y");
				Point[] verts = new Point[xVerts.size()];
				for(int i = 0; i < xVerts.size();i++){
					verts[i] = new Point(Integer.parseInt(xVerts.get(i).toString()),Integer.parseInt(yVerts.get(i).toString()));
				}
				if(jo.containsKey("texture")){
					try {
						img = ImageIO.read(new File(jo.get("texture").toString()));
					} catch (IOException ex) {
					}
					funt = img;
				}
				Character ch = new Character(posX, posY, name, jo.get("texture").toString());
				
				if(jo.containsKey("hitbox")){
	            	JSONObject hitboxJ = (JSONObject) jo.get("hitbox");
	            	String type = hitboxJ.get("type").toString();
	            	if(type.compareTo("circle")==0){
	    				JSONArray centre  = (JSONArray)hitboxJ.get("centre");
	    				int cx = Integer.parseInt(centre.get(0).toString());
	    				int cy = Integer.parseInt(centre.get(1).toString());
	    	            int diameter = Integer.parseInt(hitboxJ.get("diameter").toString());
	    	            
	    	            Ellipse2D.Float playerCirc = new Ellipse2D.Float();
	    	    		playerCirc.setFrame(cx, cy, diameter,diameter);
	    	    		playerCirc.x = (float) cx;
	    	    		playerCirc.y = (float) cy;
	    	    		ch.centre.x = cx;
	    	    		ch.centre.y = cy;
	    	    		ch.playerCirc = playerCirc;
	            	}
	            }
				if(jo.containsKey("equippedWeapon")){
					String weapon = jo.get("equippedWeapon").toString();
					if(weapon.compareTo("gun")==0){
						Gun gun = new Gun(weapon);
						ch.equippedItem = gun;
					}
				}
				if(jo.containsKey("equippedArmour")){
					String armour = jo.get("equippedArmour").toString();
					for(Object ob:this.armourPresets){
						JSONObject jpreset = (JSONObject) ob;
						String currentName = jpreset.get("name").toString();
						if(currentName.compareTo(armour)==0){
							String wfilename = jpreset.get("wearingFilename").toString();
							String fac = jpreset.get("faction").toString();
							double pen = Double.parseDouble(jpreset.get("penDef").toString());
							double blunt = Double.parseDouble(jpreset.get("bluntDef").toString());
							Armour a = new Armour(name, wfilename, fac, pen,blunt);
							ch.equippedArmour = a;
						}
					}
				}
				
				if(jo.containsKey("factions")){
					JSONObject factions = (JSONObject)jo.get("factions");
					if(factions != null){
						JSONArray hostile = (JSONArray)factions.get("hostile");
						for(int ho = 0; ho < hostile.size(); ho ++){
							String fac = hostile.get(ho).toString();
							ch.brain.hostileFactions.add(fac);
						}
						
						JSONArray allied = (JSONArray)factions.get("allied");
						for(int al = 0; al < allied.size(); al ++){
							String fac = allied.get(al).toString();
							ch.brain.alliedFactions.add(fac);
						}
						
						JSONArray friendly = (JSONArray)factions.get("friendly");
						for(int fr = 0; fr < friendly.size(); fr ++){
							String fac = friendly.get(fr).toString();
							ch.brain.friendlyFactions.add(fac);
						}
					}
				}
				
				JSONArray behaviours = (JSONArray)jo.get("behaviours");
				if(behaviours != null){
					ch.brain.readBehaviours(behaviours);					
				}
				
				charList.add(ch);
			}
			
			JSONArray groundObjectsJSON = (JSONArray) jsonObject.get("groundObjects");
			for(Object gJSON: groundObjectsJSON){
				JSONObject jo = (JSONObject)gJSON;
				String name  = jo.get("name").toString();
				JSONArray pos  = (JSONArray)jo.get("position");
				int posX = Integer.parseInt(pos.get(0).toString());
				int posY = Integer.parseInt(pos.get(1).toString());
				String preset = jo.get("preset").toString();
				for(Object ob:this.armourPresets){
					JSONObject jpreset = (JSONObject) ob;
					String currentName = jpreset.get("name").toString();
					if(currentName.compareTo(preset)==0){
						String wfilename = jpreset.get("wearingFilename").toString();
						String fac = jpreset.get("faction").toString();
						double pen = Double.parseDouble(jpreset.get("penDef").toString());
						double blunt = Double.parseDouble(jpreset.get("bluntDef").toString());
						Armour a = new Armour(name, wfilename, fac, pen,blunt);
						
						GroundItem gi = new GroundItem(posX,posY,name,"dropItem.png");
						gi.setItem(a);
						groundItemsList.add(gi);
						
					}
				}
				
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		generateNodeGraph();
	}
	
	public void generateNodeGraph(){
		this.currentArea.getBounds();
		int DIST_BETWEEN_NODES = 20;
		
		int width = this.currentArea.getBounds().xpoints[1] - this.currentArea.getBounds().xpoints[0];
		int height = this.currentArea.getBounds().ypoints[3] - this.currentArea.getBounds().ypoints[0];

		int numHorizontal = width/DIST_BETWEEN_NODES;
		int numVertical = height/DIST_BETWEEN_NODES;

		for(int i = 1; i < numHorizontal - 1; i ++){
			for(int j = 1; j < numVertical - 1; j++){
				this.nodeGraph.addNode(this.currentArea.getBounds().xpoints[0] + i*DIST_BETWEEN_NODES, 
						this.currentArea.getBounds().ypoints[0] + j*DIST_BETWEEN_NODES);
			}
		}
		System.out.println("NBLL");
		ArrayList<String> nodesToDelete = new ArrayList<String>();
		for(String n: this.nodeGraph.nodes.keySet()){
			Node currentN = this.nodeGraph.nodes.get(n);
			Point nPoint = new Point(currentN.xPos,currentN.yPos);
			for(Entity e: entityList){
				if(e.collideable){
					Polygon poly = new Polygon();
					for(Point p : e.getTranslatedVertexes()){
						poly.addPoint(p.x,p.y);
					}
					if(poly.contains(nPoint)){
						nodesToDelete.add(currentN.name);
					}
				}
			}
		}
		System.out.println(this.nodeGraph.nodes.size());
		for(String s: nodesToDelete){
			this.nodeGraph.nodes.remove(s);
		}
		System.out.println(this.nodeGraph.nodes.size());

		System.out.println("DONE");
		
		for(String n: this.nodeGraph.nodes.keySet()){
			//LEFT
			String name = "Node("+ (this.nodeGraph.nodes.get(n).xPos-DIST_BETWEEN_NODES) +"," +(this.nodeGraph.nodes.get(n).yPos) + ")";
			if(this.nodeGraph.nodes.containsKey(name)){
				Point pA = new Point(this.nodeGraph.nodes.get(n).xPos,this.nodeGraph.nodes.get(n).yPos);
				Point pB = new Point(this.nodeGraph.nodes.get(name).xPos,this.nodeGraph.nodes.get(name).yPos);
				if(!doesEdgeCutEntity(pA,pB)){
					//System.out.println("MAKING");
					this.nodeGraph.addEdge(n, name);
				}
			}
			//RIGHT
			name = "Node("+ (this.nodeGraph.nodes.get(n).xPos+DIST_BETWEEN_NODES) +"," +(this.nodeGraph.nodes.get(n).yPos) + ")";
			if(this.nodeGraph.nodes.containsKey(name)){
				Point pA = new Point(this.nodeGraph.nodes.get(n).xPos,this.nodeGraph.nodes.get(n).yPos);
				Point pB = new Point(this.nodeGraph.nodes.get(name).xPos,this.nodeGraph.nodes.get(name).yPos);
				if(!doesEdgeCutEntity(pA,pB)){
					//System.out.println("MAKING");
					this.nodeGraph.addEdge(n, name);
				}			
			}
			//UP
			name = "Node("+ (this.nodeGraph.nodes.get(n).xPos) +"," +(this.nodeGraph.nodes.get(n).yPos - DIST_BETWEEN_NODES) + ")";
			if(this.nodeGraph.nodes.containsKey(name)){
				Point pA = new Point(this.nodeGraph.nodes.get(n).xPos,this.nodeGraph.nodes.get(n).yPos);
				Point pB = new Point(this.nodeGraph.nodes.get(name).xPos,this.nodeGraph.nodes.get(name).yPos);
				if(!doesEdgeCutEntity(pA,pB)){
					//System.out.println("MAKING");
					this.nodeGraph.addEdge(n, name);
				}			
			}
			//DOWN
			name = "Node("+ (this.nodeGraph.nodes.get(n).xPos) +"," +(this.nodeGraph.nodes.get(n).yPos + DIST_BETWEEN_NODES) + ")";
			if(this.nodeGraph.nodes.containsKey(name)){
				Point pA = new Point(this.nodeGraph.nodes.get(n).xPos,this.nodeGraph.nodes.get(n).yPos);
				Point pB = new Point(this.nodeGraph.nodes.get(name).xPos,this.nodeGraph.nodes.get(name).yPos);
				if(!doesEdgeCutEntity(pA,pB)){
					//System.out.println("MAKING");
					this.nodeGraph.addEdge(n, name);
				}
			}
			//UP RIGHT
			name = "Node("+ (this.nodeGraph.nodes.get(n).xPos + DIST_BETWEEN_NODES) +"," +(this.nodeGraph.nodes.get(n).yPos - DIST_BETWEEN_NODES) + ")";
			if(this.nodeGraph.nodes.containsKey(name)){
				Point pA = new Point(this.nodeGraph.nodes.get(n).xPos,this.nodeGraph.nodes.get(n).yPos);
				Point pB = new Point(this.nodeGraph.nodes.get(name).xPos,this.nodeGraph.nodes.get(name).yPos);
				if(!doesEdgeCutEntity(pA,pB)){
					//System.out.println("MAKING");
					this.nodeGraph.addEdge(n, name);
				}
			}
			//RIGHT DOWN
			name = "Node("+ (this.nodeGraph.nodes.get(n).xPos + DIST_BETWEEN_NODES) +"," +(this.nodeGraph.nodes.get(n).yPos + DIST_BETWEEN_NODES) + ")";
			if(this.nodeGraph.nodes.containsKey(name)){
				Point pA = new Point(this.nodeGraph.nodes.get(n).xPos,this.nodeGraph.nodes.get(n).yPos);
				Point pB = new Point(this.nodeGraph.nodes.get(name).xPos,this.nodeGraph.nodes.get(name).yPos);
				if(!doesEdgeCutEntity(pA,pB)){
					//System.out.println("MAKING");
					this.nodeGraph.addEdge(n, name);
				}
			}
			//DOWN LEFT
			name = "Node("+ (this.nodeGraph.nodes.get(n).xPos - DIST_BETWEEN_NODES) +"," +(this.nodeGraph.nodes.get(n).yPos + DIST_BETWEEN_NODES) + ")";
			if(this.nodeGraph.nodes.containsKey(name)){
				Point pA = new Point(this.nodeGraph.nodes.get(n).xPos,this.nodeGraph.nodes.get(n).yPos);
				Point pB = new Point(this.nodeGraph.nodes.get(name).xPos,this.nodeGraph.nodes.get(name).yPos);
				if(!doesEdgeCutEntity(pA,pB)){
					//System.out.println("MAKING");
					this.nodeGraph.addEdge(n, name);
				}
			}
			//UP LEFT
			name = "Node("+ (this.nodeGraph.nodes.get(n).xPos - DIST_BETWEEN_NODES) +"," +(this.nodeGraph.nodes.get(n).yPos - DIST_BETWEEN_NODES) + ")";
			if(this.nodeGraph.nodes.containsKey(name)){
				Point pA = new Point(this.nodeGraph.nodes.get(n).xPos,this.nodeGraph.nodes.get(n).yPos);
				Point pB = new Point(this.nodeGraph.nodes.get(name).xPos,this.nodeGraph.nodes.get(name).yPos);
				if(!doesEdgeCutEntity(pA,pB)){
					//System.out.println("MAKING");
					this.nodeGraph.addEdge(n, name);
				}
			}
		}
	}
	
	public Node findClosestNode(Point p){
		String nameOfClosestNode = "";
		double minDist = Double.MAX_VALUE;
		for(String s: this.nodeGraph.nodes.keySet()){
			Node n = this.nodeGraph.nodes.get(s);
			double currentDist = p.distance(new Point(n.xPos,n.yPos));
			if(currentDist < minDist){
				minDist = currentDist;
				nameOfClosestNode = s;
			}
		}
		return this.nodeGraph.nodes.get(nameOfClosestNode);
	}
	
	public boolean doesEdgeCutEntity(Point p1, Point p2){
		boolean answer = false;
		Line2D edge = new Line2D.Float(p1,p2);
		for(Entity e:entityList){
			Point[] ev = e.vertexes;
			for(int i = 0; i < ev.length;i++){
				Line2D.Float l2;
				if(i == ev.length - 1){
					l2 = new Line2D.Float(ev[i],ev[0]);
				}
				else{
					l2 = new Line2D.Float(ev[i],ev[i+1]);
				}
				if(l2.intersectsLine(edge)){
					answer = true;
					//System.out.println("COULDNT MAKE EDGE" + edge);
				}
			}
		}
		return answer;
	}
	
	public void loadEvents(String filename) {
		ArrayList<String> al = new ArrayList<String>();
		try {
			File file = new File(filename);
			FileReader fileReader = new FileReader(file);
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			StringBuffer stringBuffer = new StringBuffer();
			String line;
			while ((line = bufferedReader.readLine()) != null) {
				al.add(line);
			}
			fileReader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		for (int l = 0; l < al.size(); l++) {
			String name = al.get(l);
			l++;
			String trig = al.get(l);
			String[] trigStrings = trig.split("\\>");
			if (trigStrings[0].compareTo("LOCATION") == 0) {
				String level = al.get(l);
				String[] splitp = trigStrings[2].split(",");
				int x = Integer.parseInt(splitp[0]);
				int y = Integer.parseInt(splitp[1]);
				Trigger t = new Trigger(triggerType.LOCATION);
				int ammo = Integer.parseInt(trigStrings[3]);
				t.setAmmo(ammo);
				t.location = new Point(x, y);
				t.level = trigStrings[1];
				t.name = name;
				l++;
				String ev = al.get(l);
				while (ev.length() > 1) {
					t.addTargetEvent(new Event(ev));
					this.eventsList.add(new Event(ev));
					l++;
					if (l >= al.size()) {
						break;
					}
					ev = al.get(l);
				}
				locTriggerList.add(t);

			}
			if (trigStrings[0].compareTo("CONVO") == 0) {
				l++;
				String ev = al.get(l);
				Trigger t = new Trigger(triggerType.CONVO);
				while (ev.length() > 1) {
					t.addTargetEvent(new Event(ev));
					this.eventsList.add(new Event(ev));
					l++;
					if (l >= al.size()) {
						break;
					}
					ev = al.get(l);
				}
				t.name = name;
				convoTriggerList.add(t);
			}

		}
	}
	
	@Override
	public void mouseClicked(MouseEvent arg0) {
		int mButton = arg0.getButton();
		if(mButton == 1){
			clicked = true;
		}
		if(mButton == 3){
			rightClicked = true;
		}
		//clicked = true;
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		int mButton = e.getButton();
		if(mButton == 1){
			keyb.lmb = true;
		}
		if(leftReleased == true){
			leftPressed = true;
			leftReleased = false;
			System.out.println("LEFTPRESSED");
		}
		else{
			leftPressed = false;
			System.out.println("LEFTNOTPRESSED");

		}
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		int mButton = e.getButton();
		if(mButton == 1){
			keyb.lmb = false;
		}
		leftReleased = true;
		System.out.println("LEFTRELEASED");

		
		// TODO Auto-generated method stub

	}

}