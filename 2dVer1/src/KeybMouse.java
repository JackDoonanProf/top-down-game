
public class KeybMouse {
	public boolean up;
	public boolean down;
	public boolean left;
	public boolean right;
	public boolean e;
	public boolean esc;
	public boolean shift;
	public boolean space;
	public boolean ctrl;
	public boolean lmb;
	public boolean rmb;
	
	public boolean w;
	public boolean a;
	public boolean s;
	public boolean d;
	
	public int leftClickState;
	
	public int xCoord;
	public int yCoord;
	
	public KeybMouse(){
		up = false;
		down = false;
		left= false;
		right = false;
		e = false;
		esc = false;
		shift = false;
		ctrl = false;
		space = false;
		lmb = false;
		rmb = false;
		w = false;
		a = false;
		s= false;
		d = false;
		
	}
	
	public void reset(){
		up = false;
		down = false;
		left= false;
		right = false;
		e = false;
		esc = false;
		shift = false;
		ctrl = false;
		space = false;
		lmb = false;
		rmb = false;
		w = false;
		a = false;
		s= false;
		d = false;
	}
	
}
