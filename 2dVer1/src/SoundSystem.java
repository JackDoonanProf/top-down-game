import java.awt.Point;
import java.util.ArrayList;
import java.util.HashMap;

public class SoundSystem {
	private static SoundSystem instance;
	
	HashMap<String,Sound> sounds = new HashMap<String,Sound>();
	
	public static SoundSystem getInstance(){
		if(instance != null){
			return instance;
		}
		else{
			instance = new SoundSystem();
			return instance;
		}
	}
	
	public void addSound(Sound s){
		this.sounds.put(s.name, s);
	}
	
	public void update(){
		ArrayList<String> soundsToKill = new ArrayList<String>();
		for(String s: this.sounds.keySet()){
			this.sounds.get(s).update();
			if(!this.sounds.get(s).alive){
				soundsToKill.add(s);
			}
		}
		for(String s: soundsToKill){
			this.sounds.remove(s);
		}
	}
	
	public HashMap<String,Point> getSoundsAtPoint(Point p,double threshold){
		HashMap<String,Point> soundsHeard = new HashMap<String,Point>();
		for(String s: this.sounds.keySet()){
			double volume = this.getRelativeVolume(s, p);
			if(volume >= threshold){
				soundsHeard.put(s,this.sounds.get(s).origin);
			}
		}
		return soundsHeard;
	}
	
	
	public double getRelativeVolume(String soundName, Point p){
		double result = 0;
		if(this.sounds.containsKey(soundName)){
			int v = this.sounds.get(soundName).volume;
			double distance = p.distance(this.sounds.get(soundName).origin);
			result = v * (1/(distance * distance));
			return result;
		}else return result;
	}
	
}
