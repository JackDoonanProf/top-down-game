import java.awt.Point;
import java.awt.Polygon;

public class Area {
	Point[] bounds;
	String name;
	
	public Area(int[] xs, int[] ys,String n){
		bounds = new Point[xs.length];
		name = n;
		for(int i = 0; i < xs.length; i++){
			bounds[i] = new Point(xs[i],ys[i]);
		}
	}
	
	public Polygon getBounds(){
		Polygon P = new Polygon();
		for(int i = 0; i < bounds.length; i++){
			P.addPoint(bounds[i].x, bounds[i].y);
		}
		
		return P;
	}
}
