import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

public class Explosion extends Entity {

	int size = 10;
	Ellipse2D.Float damageCircle;
	double lifetime = 500;
	long birthDay = 0;
	boolean dead = false;
	
	public Explosion(int x, int y, String n,int size) {
		super(x, y, n);
		this.size = size;
		this.damageCircle = new Ellipse2D.Float();
		this.damageCircle.setFrameFromDiagonal(new Point(0,0), new Point(size,size));
		this.damageCircle.x = (float) this.xPos;
		this.damageCircle.y = (float) this.yPos;
		this.vertexes = new Point[1];
		this.vertexes[0] = new Point(0,0);
		this.birthDay = GameClock.getInstance().currentTime;
		// TODO Auto-generated constructor stub
	}
	public Graphics draw(Graphics g, GameLoopTest.GamePanel gp, Camera c){
		g.setColor(Color.RED);
		Graphics2D g2d = (Graphics2D)g;
		this.damageCircle.x -= c.x;
		this.damageCircle.y -= c.y;
		g2d.fill(damageCircle);
		this.damageCircle.x += c.x;
		this.damageCircle.y += c.y;
		return g;
	}
	public String update(ArrayList<Entity> entList,ArrayList<Character> charList, ArrayList<Door> doorList){
		String command = "";
		this.centre = new Point((int)this.xPos + (size/2),(int)this.yPos + (size/2));
		for(Character c: charList){
			if(centre.distance(c.centre) < (size/2) + c.halfX){
				c.Damage(5);
			}
		}
		for(Door d: doorList){
			if(centre.distance(d.centre) < (size/2) + d.halfX){
				d.Damage(5);
			}
		}
		return command;
	}
	public void checkTimer(){
		long timeElapsed = GameClock.getInstance().currentTime;
		if(timeElapsed - this.birthDay >= this.lifetime){
			this.dead = true;
		}
	}

}
