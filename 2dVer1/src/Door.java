import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

public class Door extends Entity {

	int openPos = 90;
	int closedPos = 0;
	int width = 0;
	int depth = 0;
	boolean open = true;
	boolean destructable = false;
	int health = -1;
	//REPRESENTS A DOOR THAT CAN SWING OPEN.
	//THE X AND Y COORDINATES ARE THE POSITION WE ROTATE AROUND
	//THINK OF IT AS THE HINGE POISiON
	public Door(int x, int y, String n,int w, int d, int c, int o, int health, boolean destructable) {
		super(x, y, n);
		this.col = Color.GREEN;
		this.vertexes = new Point[4];
		this.vertexes[0] = new Point(x,y);
		this.vertexes[1] = new Point(x + d,y);
		this.vertexes[2] = new Point(x + d,y +w);
		this.vertexes[3] = new Point(x,y + w);
		
		this.health = health;
		this.destructable = destructable;
		this.closedPos = c;
		this.openPos = o;
		this.rotation = this.closedPos;
		Polygon poly = new Polygon();
		Point[] rotated = new Point[vertexes.length];
		for(int i = 0; i < vertexes.length;i++){
			rotated[i] = new Point(0,0);
		}
		rotatePointMatrix(vertexes, rotation,rotated,new Point((int)x,(int)y));
		this.vertexes = rotated;
		
		this.open = false;
		centre = findCentre(vertexes);
		
		this.collideable = true;
		// TODO Auto-generated constructor stub
	}
	
	public Point findCentre(Point[] ver){
		//for(int i = 0; i < ver.length;i++){
		//	ver[i].x = ver[i].x + (int)xPos;
		//	ver[i].y = ver[i].y + (int)yPos;
		//}
		double sumX = 0;
		double sumY = 0;
		for(int i = 0; i < ver.length;i++){
			sumX += ver[i].x;
			sumY += ver[i].y;
		}
		Point res = new Point((int)(sumX/ver.length),(int)(sumY/ver.length));
		
		return res;
	}
	
	public void setClosedPos(int rot) {
		this.rotation = rot;
		this.closedPos = rot;
	}
	public void update(KeybMouse keybstar){
		super.update(keybstar);
	}
	
	public int calculateLeftClickState(Point mouse,boolean click){
		if(click){
			System.out.println("CLICK");
		}
		int lastState = this.clickState;
		int c = 0;
		
		Polygon thispoly = new Polygon();
		for(int i = 0; i < vertexes.length;i++){
			thispoly.addPoint(this.vertexes[i].x,this.vertexes[i].y);
		}
		if(thispoly.contains(mouse)){
			c = 1;
			if(click){
				c = 2;
			}
		 }
		
		
		//releasing after clicking on object
		if(lastState == 2 && c == 1)return 3;
		//Hovering
		if(c == 1)return c;
		//Clicking Down
		if(c == 2)return c;
		//releasing away from the object
		return 0;
	}
	
	public void setOpenPos(int rot) {
		this.openPos = rot;
	}
	
	public void setOpen(boolean open) {
		this.open = open;
	}
	
	public ArrayList<String> getClickOptions(){
		ArrayList<String> al = new ArrayList<String>();
		al.add("open door");
		al.add("close door");
		al.add("unlock");
		return al;
	}
	
	public void open(){
		if(this.open){
			return;
		}
		this.open = true;
		this.rotation = this.openPos;
		Polygon poly = new Polygon();
		Point[] rotated = new Point[vertexes.length];
		for(int i = 0; i < vertexes.length;i++){
			rotated[i] = new Point(0,0);
		}
		rotatePointMatrix(vertexes, rotation,rotated,new Point((int)this.xPos,(int)this.yPos));
		this.vertexes = rotated;
	}
	public void close(){
		if(!this.open){
			return;
		}
		this.open = false;
		this.rotation = - this.openPos;
		Polygon poly = new Polygon();
		Point[] rotated = new Point[vertexes.length];
		for(int i = 0; i < vertexes.length;i++){
			rotated[i] = new Point(0,0);
		}
		rotatePointMatrix(vertexes, rotation,rotated,new Point((int)this.xPos,(int)this.yPos));
		this.vertexes = rotated;
		
	}
	
	public Point[] getTranslatedVertexes(){
		return this.vertexes;
	}
	
	public Graphics draw(Graphics g, GameLoopTest.GamePanel gp, Camera c){
		centre = findCentre(vertexes);
		Polygon poly = new Polygon();
		Point[] rotated = new Point[vertexes.length];
		for(int i = 0; i < vertexes.length;i++){
			poly.addPoint(vertexes[i].x - c.x,vertexes[i].y - c.y);
		}
		g.setColor(this.col);
		g.fillPolygon(poly);
		return g;
	}

	public void Damage(int i) {
		// TODO Auto-generated method stub
		if(this.destructable == true){
			this.health -= i;
		}
	}

}
