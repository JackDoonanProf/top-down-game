import java.awt.Point;
import java.util.ArrayList;

public class Trigger {
	public triggerType type;
	
	public ArrayList<Event> targetEvents;
	
	public Point location;
	
	public String level;
	
	public String name;
	int ammo;
	
	public Trigger(triggerType t){
		this.type = t;
		targetEvents = new ArrayList<Event>();
	}
	
	public void setLocation(Point l){
		this.location = l;
	}
	
	public void setAmmo(int a){
		this.ammo = a;
	}
	public void setLevel(String l){
		this.level = l;
	}
	
	public void addTargetEvent(Event e){
		this.targetEvents.add(e);
	}
	public void decrement(){
		//RETURN TRUE IF THE THING IS OUT OF TRIGS
		this.ammo --;

	}
	
	public boolean isDead(){
		if(ammo == 0){
			return true;
		}
		return false;
	}
	
}
