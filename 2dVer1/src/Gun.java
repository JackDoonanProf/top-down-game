import java.awt.Color;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Gun extends Weapon {

	double firingVelocity;
	
	public Gun(String n) {
		super(n);
		Point p1 = new Point(0,0);
		Point p2 = new Point(0,5);
		Point p3 = new Point(5,5);
		Point p4 = new Point(5,0);
		Point[] points = new Point[4];
		points[0] = p1;
		points[1] = p2;
		points[2] = p3;
		points[3] = p4;
		this.proj = new Projectile(0,0,"Bullet",points,Color.BLUE,null,0,2);
		this.firingVelocity = 100;
		this.clipSize = 15;
		this.loadedAmmunition = 0;
		BufferedImage funt = null;
		iconFilename = "gunStandardIcon.png";
		try {
			funt = ImageIO.read(new File("gunStandardIcon.png"));
			
		} catch (IOException e) {
		}
		this.icon = funt;
	}
	
	@Override
	public Command use(Point mouse,double x, double y){
		if((GameClock.getInstance().currentTime - this.lastTimeFired) >= this.fireInterval){
			this.lastTimeFired = GameClock.getInstance().currentTime;
		}else{
			return new Command("firingTooQuick");
		}
		if(loadedAmmunition <= 0){
			return new Command("outOfAmmo");
		}
		this.loadedAmmunition --;
		SoundSystem.getInstance().addSound( new Sound("gunshot", 1000, 200, new Point((int)x,(int)y) ));
		return new Command("fire" + ">" + proj.name + ">" + firingVelocity);
	}

}
