import java.awt.Color;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Bow extends Gun {
	double firingVelocity;
	
	public Bow(String n) {
		super(n);
		Point p1 = new Point(0,0);
		Point p2 = new Point(0,5);
		Point p3 = new Point(5,5);
		Point p4 = new Point(5,0);
		Point[] points = new Point[4];
		points[0] = p1;
		points[1] = p2;
		points[2] = p3;
		points[3] = p4;
		this.proj = new Projectile(0,0,"Arrow",points,Color.BLUE,null,0,5);
		this.firingVelocity = 80;
		this.clipSize = 1;
		this.loadedAmmunition = 0;
		BufferedImage funt = null;
		try {
			funt = ImageIO.read(new File("bowIcon.png"));
		} catch (IOException e) {
		}
		this.icon = funt;
		iconFilename = "bowIcon.png";
	}
	
	public void reload(int numBullets){
		this.loadedAmmunition += numBullets;
	}
	@Override
	public Command use(Point mouse,double x, double y){
		if(loadedAmmunition <= 0){
			return new Command("outOfAmmo");
		}
		this.loadedAmmunition --;
		return new Command("fire" + ">" + proj.name + ">" + firingVelocity);
	}
}
