import java.awt.Graphics;
import java.util.Date;

public class Dialogue {
	double xPos;
	double yPos;
	String text;
	
	long startTime;
	
	long duration;
	
	public boolean finished = false;
	
	public Dialogue(String t, int y,int x, long d){
		xPos = x;
		yPos = y;
		text = t;
		duration = d;
		startTime = System.currentTimeMillis()/1000;
	}
	
	public Graphics draw(Graphics g){
		g.drawString(text, (int)xPos, (int)yPos);
		return g;
	}
	
	public void update(){
		long elapsed = System.currentTimeMillis()/1000- startTime;
		if(elapsed >= duration){
			finished = true;
		}
	}
}
