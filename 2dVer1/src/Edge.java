
public class Edge {

	public Node start;
	public Node end;
	public double weight;
	
	public Edge(Node s, Node e, double w){
		this.start = s;
		this.end = e;
		this.weight = w;
	}
}
