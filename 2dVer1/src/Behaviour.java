import java.awt.Point;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Stack;
import java.util.LinkedList;
import java.util.Queue;

import org.json.simple.*;

public class Behaviour {
	
	String name;
	String onComplete;
	String type;
	
	Point position;
		
	double rotation;
	
	boolean nodeStarted = false;
	long timeStarted = 0;

	Brain brain;
	
	ArrayList<BehaviourNode> nodes = new ArrayList<BehaviourNode>();
	ArrayList<BehaviourNode> originalNodes = new ArrayList<BehaviourNode>();

	int currentNode;
	public Behaviour(String n, String onCom, String t,Brain b){
		this.name = n;
		this.onComplete = onCom;
		this.type = t;
		this.position = new Point(0,0);
		this.brain = b;
	}
	
	public void reset(){
		nodeStarted = false;
		timeStarted = 0;
		currentNode = 0;
		this.nodes = new ArrayList<BehaviourNode>();
		for(BehaviourNode b: this.originalNodes){
			this.nodes.add(b);
		}
	}
	public String updateNode(Point p,double rot){
		String command = "";
		this.position = p;
		this.rotation = rot;
		if(this.type.compareTo("seek") == 0){
			if(currentNode >= nodes.size()){
				return "complete,moveto,"+this.onComplete;
			}
			if(this.nodes.get(currentNode).data.get("type").compareTo("seekPosition")==0 ){
				String sPosition = this.nodes.get(currentNode).data.get("position");
				sPosition = sPosition.substring(1, sPosition.length() - 1);
				String[] splitPosition = sPosition.split(",");
				int xGoal = Integer.parseInt(splitPosition[0]);
				int yGoal = Integer.parseInt(splitPosition[1]);
				ArrayList<BehaviourNode> tempNodes = new ArrayList<BehaviourNode>();
				for(int i = currentNode + 1; i < this.nodes.size();i++){
					tempNodes.add(this.nodes.get(i));
				}
				this.nodes = new ArrayList<BehaviourNode>();
				ArrayList<BehaviourNode> path = this.getPath(new Point(xGoal,yGoal));
				
				
				for(BehaviourNode b: path){
					this.nodes.add(b);
				}
				for(BehaviourNode b:tempNodes){
					this.nodes.add(b);
				}
				this.currentNode = 0;
			}
		}
		if(this.type.compareTo("flee") == 0){
			return this.updateFleeNode();
		}
		
		/*
		if(this.nodes.size() == 0){
			if(this.type.compareTo("seekHome")==0){
				this.nodes = this.getPath(this.brain.home);
			}
		}
		else{
			if(this.type.compareTo("seekHome")==0){
				if(currentNode >= nodes.size()){
					return "complete,moveto,"+this.onComplete;
				}
				return this.updateSeekHomeNode();
			}
		}*/
		
		if(currentNode >= nodes.size()){
			return "complete,moveto,"+this.onComplete;
		}
		System.out.println(currentNode + nodes.get(currentNode).data.get("type"));
		if(!nodeStarted){
			nodeStarted = true;
			timeStarted = GameClock.getInstance().currentTime;
		}
		if(this.type.compareTo("patrol")==0){
			if(this.nodes.get(currentNode).data.get("type").compareTo("seekPosition")==0 ){
				String sPosition = this.nodes.get(currentNode).data.get("position");
				sPosition = sPosition.substring(1, sPosition.length() - 1);
				String[] splitPosition = sPosition.split(",");
				int xGoal = Integer.parseInt(splitPosition[0]);
				int yGoal = Integer.parseInt(splitPosition[1]);
				ArrayList<BehaviourNode> tempNodes = new ArrayList<BehaviourNode>();
				for(int i = currentNode + 1; i < this.nodes.size();i++){
					tempNodes.add(this.nodes.get(i));
				}
				this.nodes = new ArrayList<BehaviourNode>();
				ArrayList<BehaviourNode> path = this.getPath(new Point(xGoal,yGoal));
				for(BehaviourNode b: path){
					this.nodes.add(b);
				}
				for(BehaviourNode b:tempNodes){
					this.nodes.add(b);
				}
				this.currentNode = 0;
			}
			return this.updatePatrolNode();
		}
		if(this.type.compareTo("seek") == 0){
			return this.updateSeekHomeNode();
		}
		return command;
	}
	
	String updateFleeNode(){
		if(nodes.get(currentNode).data.get("type").compareTo("moveAway") == 0){
			int lookTime = Integer.parseInt(nodes.get(currentNode).data.get("lookTime"));
			
			if(!nodes.get(currentNode).data.containsKey("startTime")){
				nodes.get(currentNode).data.put("startTime", GameClock.getInstance().currentTime.toString());
			}
			Long startTime = Long.parseLong(nodes.get(currentNode).data.get("startTime"));
			Long difference = GameClock.getInstance().currentTime - startTime;
			if(difference >= Long.parseLong(nodes.get(currentNode).data.get("lookTime"))){
				this.currentNode ++;
				nodeStarted = false;
				timeStarted = -1;
				return "";
			}
			double angletoTurn = calculateRotationFromMouse(this.brain.fleeingFrom,this.position);			
			if((angletoTurn - 180) < 0){
				angletoTurn = 360 + (angletoTurn - 180);
			}
			if(this.rotation != angletoTurn){
				return "face," + (angletoTurn);	
			}else{
				return this.updateMoveAwayNode(angletoTurn);
			}
		}
		if(nodes.get(currentNode).data.get("type").compareTo("lookForHostile") == 0){
			//return this.updatePatrolHoldNode();
			
		}
		
		return "";
	}
	
	String updateMoveAwayNode(double angle){
		double yMove = 13 * Math.sin(Math.toRadians(angle));
		double xMove = -13 * Math.cos(Math.toRadians(angle));
		return "move," + xMove + "," + yMove;
	}
	
	public ArrayList<BehaviourNode> collapsePath(ArrayList<BehaviourNode> a){
		ArrayList<BehaviourNode> result = new ArrayList<BehaviourNode>();		
		a.remove(a.get(0));
		BehaviourNode concatenatedNode = new BehaviourNode(a.get(0).data);
		String Sposition = concatenatedNode.data.get("position");
		//GOOD THING TO OPTIMISE OUT HERE BUT WONT BOTHER NOW
		Sposition = Sposition.substring(1, Sposition.length() - 1);
		String[] splitPosition = Sposition.split(",");
		int xGoal = Integer.parseInt(splitPosition[0]);
		int yGoal = Integer.parseInt(splitPosition[1]);
		double angletoTurn = calculateRotationFromMouse(new Point(xGoal,yGoal),this.position);
		Point storedPos = new Point(xGoal,yGoal);
		int i = 0;
		a.remove(a.get(0));
		while(a.size() > 0){
			if(i >= a.size()){
				return result;
			}
			BehaviourNode currentNode = a.remove(i);
			Sposition = currentNode.data.get("position");
			Sposition = Sposition.substring(1, Sposition.length() - 1);
			splitPosition = Sposition.split(",");
			xGoal = Integer.parseInt(splitPosition[0]);
			yGoal = Integer.parseInt(splitPosition[1]);
			double currentAngletoTurn = calculateRotationFromMouse(new Point(xGoal,yGoal),storedPos);
			if(currentAngletoTurn == angletoTurn){
				concatenatedNode.data.put("position", currentNode.data.get("position") );
				if(a.size() == 0){
					BehaviourNode newB = new BehaviourNode((HashMap)concatenatedNode.data.clone());
					result.add(newB);
					return result;
				}
				continue;
			}else{
				angletoTurn = currentAngletoTurn;
				storedPos = new Point (xGoal,yGoal);
				BehaviourNode newB = new BehaviourNode((HashMap)concatenatedNode.data.clone());
				result.add(newB);
			}
			i++;
		}
		
		return result;
	}
	
	public String updateSeekHomeNode(){
		
		String Sposition = nodes.get(currentNode).data.get("position");
		//GOOD THING TO OPTIMISE OUT HERE BUT WONT BOTHER NOW
		Sposition = Sposition.substring(1, Sposition.length() - 1);
		String[] splitPosition = Sposition.split(",");
		int xGoal = Integer.parseInt(splitPosition[0]);
		int yGoal = Integer.parseInt(splitPosition[1]);

		double angletoTurn = calculateRotationFromMouse(new Point(xGoal,yGoal),this.position);
		System.out.println("Comparing:" + angletoTurn + " to" + this.rotation);
		if(angletoTurn != this.rotation){
			return "face," + angletoTurn;				
		}

		if(this.position.distance( new Point(xGoal,yGoal)) < 2 ){
			this.currentNode ++;
			nodeStarted = false;
			timeStarted = -1;
			return "";
		}
		if(this.position.distance( new Point(xGoal,yGoal)) < 5 ){
			double angleToSend = calculateRotationFromMouse(new Point(xGoal,yGoal),this.position);
			double xVelToSend = 2 * Math.sin(Math.toRadians(angleToSend));
			double yVelToSend = 2 * Math.cos(Math.toRadians(angleToSend));
			return "move," + xVelToSend + "," + -yVelToSend;
		}
		if(this.position.distance( new Point(xGoal,yGoal)) < 20 ){
			double angleToSend = calculateRotationFromMouse(new Point(xGoal,yGoal),this.position);
			double xVelToSend = 5 * Math.sin(Math.toRadians(angleToSend));
			double yVelToSend = 5 * Math.cos(Math.toRadians(angleToSend));
			return "move," + xVelToSend + "," + -yVelToSend;
		}
		else{
			double angleToSend = calculateRotationFromMouse(new Point(xGoal,yGoal),this.position);
			double xVelToSend = 10 * Math.sin(Math.toRadians(angleToSend));
			double yVelToSend = 10 * Math.cos(Math.toRadians(angleToSend));
			return "move," + xVelToSend + "," + -yVelToSend;
		}		
		
	}
	public String updatePatrolNode(){
		
		if(nodes.get(currentNode).data.get("type").compareTo("move") == 0){
			return this.updatePatrolMoveNode();
		}
		if(nodes.get(currentNode).data.get("type").compareTo("hold") == 0){
			return this.updatePatrolHoldNode();
		}
		if(nodes.get(currentNode).data.get("type").compareTo("turnToFace") == 0){
			return this.updatePatrolTurnNode();
		}
		return "";
	}
	
	public String updatePatrolTurnNode(){
		String Sposition = nodes.get(currentNode).data.get("position");
		//GOOD THING TO OPTIMISE OUT HERE BUT WONT BOTHER NOW
		Sposition = Sposition.substring(1, Sposition.length() - 1);
		String[] splitPosition = Sposition.split(",");
		int xGoal = Integer.parseInt(splitPosition[0]);
		int yGoal = Integer.parseInt(splitPosition[1]);
		
		double angletoTurn = calculateRotationFromMouse(new Point(xGoal,yGoal),this.position);
		System.out.println("Comparing:" + angletoTurn + " to" + this.rotation);
		if(angletoTurn > this.rotation){
			double x = 0;
			double theta = this.rotation + (360 - angletoTurn);
			theta = theta%360;
			if((theta < 5) ){
				this.currentNode ++;
				nodeStarted = false;
				timeStarted = -1;
				return "";
			}else
			{
				if(angletoTurn - this.rotation > 30){
					return "turnto," + angletoTurn + ",speed,5";				
				}
				else{
					return "turnto," + angletoTurn + ",speed,2";				
				}
			}
			
		}
		else{
			if((this.rotation - angletoTurn < 5) ){
				this.currentNode ++;
				nodeStarted = false;
				timeStarted = -1;
				return "";
			}else
			{
				if(angletoTurn - this.rotation > 30){
					return "turnto," + angletoTurn + ",speed,5";				
				}
				else{
					return "turnto," + angletoTurn + ",speed,2";				
				}
			}	
		}
	}
	
	public String updatePatrolMoveNode(){
		String Sposition = nodes.get(currentNode).data.get("position");
		//GOOD THING TO OPTIMISE OUT HERE BUT WONT BOTHER NOW
		Sposition = Sposition.substring(1, Sposition.length() - 1);
		String[] splitPosition = Sposition.split(",");
		int xGoal = Integer.parseInt(splitPosition[0]);
		int yGoal = Integer.parseInt(splitPosition[1]);

		double angletoTurn = calculateRotationFromMouse(new Point(xGoal,yGoal),this.position);
		System.out.println("Comparing:" + angletoTurn + " to" + this.rotation);
		if(angletoTurn != this.rotation){
			return "face," + angletoTurn;				
		}
		if(this.position.distance( new Point(xGoal,yGoal)) < 2 ){
			this.currentNode ++;
			nodeStarted = false;
			timeStarted = -1;
			return "";
		}
		if(this.position.distance( new Point(xGoal,yGoal)) < 5 ){
			double angleToSend = calculateRotationFromMouse(new Point(xGoal,yGoal),this.position);
			double xVelToSend = 2 * Math.sin(Math.toRadians(angleToSend));
			double yVelToSend = 2 * Math.cos(Math.toRadians(angleToSend));
			return "move," + xVelToSend + "," + -yVelToSend;
		}
		if(this.position.distance( new Point(xGoal,yGoal)) < 20 ){
			double angleToSend = calculateRotationFromMouse(new Point(xGoal,yGoal),this.position);
			double xVelToSend = 5 * Math.sin(Math.toRadians(angleToSend));
			double yVelToSend = 5 * Math.cos(Math.toRadians(angleToSend));
			return "move," + xVelToSend + "," + -yVelToSend;
		}
		else{
			double angleToSend = calculateRotationFromMouse(new Point(xGoal,yGoal),this.position);
			double xVelToSend = 10 * Math.sin(Math.toRadians(angleToSend));
			double yVelToSend = 10 * Math.cos(Math.toRadians(angleToSend));
			return "move," + xVelToSend + "," + -yVelToSend;
		}
			
	}
	
	public String updatePatrolHoldNode(){
		int holdTime  = Integer.parseInt(nodes.get(currentNode).data.get("hold"));
		Long timeDiff = GameClock.getInstance().currentTime - timeStarted;
		if(timeDiff/1000 > holdTime){
			this.currentNode ++;
			nodeStarted = false;
			timeStarted = -1;
			return "";
		}
		return "";
	}
	public void readNodeArray(JSONArray nodeArray){
		if(this.type.compareTo("seek")==0){
			this.readNodeArraySeek(nodeArray);
			return;
			//HashMap<String,String> contents = new HashMap<String,String>();
			//contents.put("seek", "["+nodeArray.get(0).toString() +"," + nodeArray.get(1).toString() +"]" );			
			//this.nodes.add(new BehaviourNode(contents));
		}
		if(this.type.compareTo("patrol")==0){
			this.readNodeArrayPatrol(nodeArray);
			return;
		}
		if(this.type.compareTo("idle")==0){
			this.readNodeArrayIdle(nodeArray);
			return;
		}
		if(this.type.compareTo("flee")==0){
			this.readNodeArrayFlee(nodeArray);
			return;
		}
	}
	
	private void readNodeArrayFlee( JSONArray nodeArray){
		for(Object j: nodeArray){
			JSONObject jo = (JSONObject)j;
			String nodeType = jo.get("type").toString();
			if(nodeType.compareTo("moveAway") == 0){
				String lookTime = "1";
				if(!jo.containsKey("lookTime"))
				{
					System.err.println("MISSING lookTime IN FLEE NODE");
				}else
				{
					lookTime = jo.get("lookTime").toString();
				}
				HashMap<String,String> data = new HashMap<String,String>();
				data.put("type", "moveAway");
				data.put("lookTime", lookTime);
				BehaviourNode n = new BehaviourNode(data);
				this.nodes.add(n);
				this.originalNodes.add(n);
			}
			if(nodeType.compareTo("lookForHostile") == 0){
				HashMap<String,String> data = new HashMap<String,String>();
				data.put("type", "lookForHostile");
				BehaviourNode n = new BehaviourNode(data);
				this.nodes.add(n);
				this.originalNodes.add(n);
			}
		}
	}
	
	private void readNodeArrayIdle( JSONArray nodeArray){
		for(Object j: nodeArray){
			JSONObject jo = (JSONObject)j;
			String nodeType = jo.get("type").toString();
			if(nodeType.compareTo("idle") == 0){
				String position = "[0,0]";
				if(!jo.containsKey("position"))
				{
					System.err.println("MISSING position IN PATROL NODE");
				}else
				{
					position = jo.get("position").toString();
				}
				HashMap<String,String> data = new HashMap<String,String>();
				data.put("type", "idle");
				data.put("position", position);
				BehaviourNode n = new BehaviourNode(data);
				this.nodes.add(n);
				this.originalNodes.add(n);
			}
		}
	}
	
	private void readNodeArraySeek( JSONArray nodeArray){
		for(Object j: nodeArray){
			JSONObject jo = (JSONObject)j;
			String nodeType = jo.get("type").toString();
			if(nodeType.compareTo("seekPosition") == 0){
				String position = "[0,0]";
				if(!jo.containsKey("position"))
				{
					System.err.println("MISSING position IN PATROL NODE");
				}else
				{
					position = jo.get("position").toString();
				}
				HashMap<String,String> data = new HashMap<String,String>();
				data.put("type", "seekPosition");
				data.put("position", position);
				BehaviourNode n = new BehaviourNode(data);
				this.nodes.add(n);
				this.originalNodes.add(n);
			}
		}
	}
	
	private void readNodeArrayPatrol(JSONArray nodeArray){
		for(Object j: nodeArray){
			JSONObject jo = (JSONObject)j;
			String nodeType = jo.get("type").toString();
			
			if(nodeType.compareTo("hold")==0){
				String hold = "10";
				String look = "0";
				String sweepFreq = "2";
				
				if(!jo.containsKey("time"))
				{
					System.err.println("MISSING hold IN PATROL NODE");
				}else
				{
					hold = jo.get("time").toString();
				}
				
				if(!jo.containsKey("look"))
				{
					System.err.println("MISSING look IN PATROL NODE");
				}else
				{
					look = jo.get("look").toString();
				}
				
				if(!jo.containsKey("sweepFreq"))
				{
					System.err.println("MISSING sweepFreq IN PATROL NODE");
				}else
				{
					sweepFreq = jo.get("sweepFreq").toString();
				}
				
				HashMap<String,String> data = new HashMap<String,String>();
				data.put("type", "hold");
				data.put("hold", hold);
				data.put("look", look);
				data.put("sweepFreq", sweepFreq);
				BehaviourNode n = new BehaviourNode(data);
				this.nodes.add(n);
				this.originalNodes.add(n);
			}
			if(nodeType.compareTo("move") == 0){
				String position = "[0,0]";
				if(!jo.containsKey("position"))
				{
					System.err.println("MISSING position IN PATROL NODE");
				}else
				{
					position = jo.get("position").toString();
				}
				
				HashMap<String,String> data = new HashMap<String,String>();
				data.put("position", position);
				data.put("type", "move");
				BehaviourNode n = new BehaviourNode(data);
				this.nodes.add(n);
				this.originalNodes.add(n);
			}
			if(nodeType.compareTo("seekPosition") == 0){
				String position = "[0,0]";
				if(!jo.containsKey("position"))
				{
					System.err.println("MISSING position IN PATROL NODE");
				}else
				{
					position = jo.get("position").toString();
				}
				HashMap<String,String> data = new HashMap<String,String>();
				data.put("type", "seekPosition");
				data.put("position", position);
				BehaviourNode n = new BehaviourNode(data);
				this.nodes.add(n);
				this.originalNodes.add(n);
			}
			if(nodeType.compareTo("turnToFace") == 0){
				String position = "[0,0]";
				if(!jo.containsKey("position"))
				{
					System.err.println("MISSING position IN PATROL NODE");
				}else
				{
					position = jo.get("position").toString();
				}
				
				HashMap<String,String> data = new HashMap<String,String>();
				data.put("position", position);
				data.put("type", "turnToFace");
				BehaviourNode n = new BehaviourNode(data);
				this.nodes.add(n);
				this.originalNodes.add(n);
			}
		}
		this.currentNode = 0;
	}
	
	
	public double calculateRotationFromMouse(Point mousePos, Point centre){
		double rot = 0;
		int relativeX = mousePos.x - centre.x;
		int relativeY = centre.y - mousePos.y;
		//System.out.println("cX" + centre.x + "mY"+ centre.y);
		//System.out.println("rel X" + relativeX + "relY"+ relativeY);
		//Q1
		if(relativeX > 0 && relativeY > 0){
			double angle = Math.atan((double)relativeX/(double)relativeY);
			angle = Math.toDegrees(angle);
			rot = angle;
		}
		//Q2
		if(relativeX > 0 && relativeY < 0){
			double angle = Math.atan((double)-relativeY/(double)relativeX);
			angle = Math.toDegrees(angle);
			rot = angle + 90;
		}
		//Q3
		if(relativeX < 0 && relativeY < 0){
			double angle = Math.atan((double)-relativeX/(double)-relativeY);
			angle = Math.toDegrees(angle);
			rot = angle + 180;
		}
		//Q4
		if(relativeX < 0 && relativeY > 0){
			double angle = Math.atan((double)relativeY/(double)-relativeX);
			angle = Math.toDegrees(angle);
			rot = angle + 270;
		}
		if(relativeY == 0 && relativeX >= 0){
			return 90;
		}
		if(relativeY == 0 && relativeX < 0){
			return 270;
		}
		if(relativeX == 0 && relativeY >= 0){
			return 0;
		}
		if(relativeX == 0 && relativeY < 0){
			return 180;
		}
		return rot;
	}
	
	ArrayList<BehaviourNode> getPath(Point target){
		ArrayList<BehaviourNode> pathNodes = new ArrayList<BehaviourNode>();
		Node start = GameLoopTest.getInstance().findClosestNode(this.brain.position);
		Node end = GameLoopTest.getInstance().findClosestNode(target);

		Stack<Node> st = PathFinder.findPath(GameLoopTest.getInstance().nodeGraph, start, end);
		while(!st.isEmpty()){
			Node n = st.pop();
			HashMap<String,String> data = new HashMap<String,String>();
			data.put("type", "move");
			data.put("position", "[" + n.xPos + "," + n.yPos + "]");
			pathNodes.add(new BehaviourNode(data));
		}
		pathNodes = collapsePath(pathNodes);
		return pathNodes;
	}

	//public void makePath
}
