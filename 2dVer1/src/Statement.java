import java.util.ArrayList;

public class Statement {
	ArrayList<Response> responses;
	String text;
	ArrayList<String> triggers;
	
	public Statement(String t){
		this.text = t;
		this.responses = new ArrayList<Response>();
		this.triggers = new ArrayList<String>();
	}
	
	public void addEvent(String e){
		this.triggers.add(e);
	}
	
	public void addResponse(Response r){
		this.responses.add(r);
	}
	
	public int getResponse(int input){
		return responses.get(input).GOTO;
	}
}
