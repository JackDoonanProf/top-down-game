import java.awt.Point;

public class Camera {

	int x;
	int y;
	int SCREEN_WIDTH;
	int SCREEN_HEIGHT;
	
	
	public Camera(int width, int height){
		this.SCREEN_WIDTH = width;
		this.SCREEN_HEIGHT = height;
	}
	
	public void updateCam(Player p, Area a){
		Point pCent = p.centre;
		int px = (int)p.xPos;
		int py = (int)p.yPos;
		
		//x = pCent.x - (SCREEN_WIDTH / 2);
		//y = pCent.y - (SCREEN_HEIGHT / 2);
		
		x = px - (SCREEN_WIDTH / 2);
		y = py - (SCREEN_HEIGHT / 2);
		if(x < 0){
			x = 0;
		}
		if(y < 0){
			y = 0;
		}
		int xw = a.bounds[2].x;
		int yh = a.bounds[2].y;
		if((y + SCREEN_HEIGHT) > yh){
			y = yh - SCREEN_HEIGHT;
		}
		if((x + SCREEN_WIDTH) > xw){
			x = xw - SCREEN_WIDTH;
		}
		
		
	}
	
	
}
