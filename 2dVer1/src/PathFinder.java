import java.util.HashMap;
import java.util.Stack;

public class PathFinder {

	/**
	 * Implements the wikipedia pseudocode for A* search algorithm.
	 * 
	 */
	public static Stack<Node> findPath(Graph g, Node start, Node end){
		if(!g.nodes.containsKey(start.name)){
			System.out.println("CANT FIND START NODE");
			return null;
		}
		if(!g.nodes.containsKey(end.name)){
			System.out.println("CANT FIND START NODE");
			return null;
		}
		HashMap<String,Node> closedSet = new HashMap<String,Node>();
		
		HashMap<String,Node> openSet = new HashMap<String,Node>();
		openSet.put(start.name, start);
		
		HashMap<Node,Node> cameFrom = new HashMap<Node,Node>();
		
		HashMap<Node,Double> gScore = new HashMap<Node,Double>();
		
		for(String n: g.nodes.keySet()){
			gScore.put(g.nodes.get(n),Double.MAX_VALUE);
		}
		gScore.put(start,0.0);
		
		HashMap<Node,Double> fScore = new HashMap<Node,Double>();
		for(String n: g.nodes.keySet()){
			fScore.put(g.nodes.get(n),Double.MAX_VALUE);
		}
		
		fScore.put(start,PathFinder.getHeuristic(start,end));
		
		while(openSet.size() > 0){
			Node current = openSet.get( openSet.keySet().toArray()[0]);
			for(String n: openSet.keySet()){
				if(fScore.get(g.nodes.get(n)) < fScore.get(current)){
					current = openSet.get(n);
				}
			}
			if(current == end){
				return createPath(cameFrom,current);
			}
			openSet.remove(current.name);
			closedSet.put(current.name,current);
			
			for(Edge e: current.connections){
				if(closedSet.containsKey(e.end.name)){
					continue;
				}
				if(!openSet.containsKey(e.end.name)){
					openSet.put(e.end.name, e.end);
				}
				double tentative_gScore = gScore.get(current) + e.weight;
				if(tentative_gScore >= gScore.get(e.end)){
					continue;
				}
				cameFrom.put(e.end,current);
				gScore.put(e.end,tentative_gScore);
				fScore.put(e.end,tentative_gScore + PathFinder.getHeuristic(e.end,end));
			}
		}
		return null;
	}
	
	public static Double getHeuristic(Node start, Node end){
		return 5.0;
	}
	
	public static Stack<Node> createPath(HashMap<Node,Node> cameFrom, Node goal){
		Stack<Node> path = new Stack<Node>();
		path.push(goal);
		Node previous = cameFrom.get(goal);
		while(previous != null){
			path.push(previous);
			previous = cameFrom.get(previous);
		}
		return path;
	}
}
