import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;

public class Entity {
	String name;
	
	Color col = Color.BLACK;
	
	Point[] vertexes;
	Point[] originalVertexes;
	Point centre;
	double xPos;
	double yPos;
	
	double scale = 1;
	
	boolean moveable = false;
	
	BufferedImage texture;
	
	int clickState;
	
	boolean collideable = false;
	
	double rotation;

	double xVel;
	double yVel;
	
	double halfX;
	double halfY;

	
	public Entity(int x,int y, String n, Point[] v, Color c, BufferedImage funt){
		xPos = x;
		yPos = y;
		name = n;
		vertexes = v;
		originalVertexes = v;
		col = c;
		rotation = 0;
		clickState = 0;
		texture = funt;
		collideable = false;
		centre = new Point(0,0);
		halfX = Math.abs(vertexes[0].x - vertexes[1].x)/2;
		halfY = Math.abs(vertexes[0].y - vertexes[2].y)/2;
	}
	public Entity(int x,int y, String n){
		xPos = x;
		yPos = y;
		name = n;
		col = Color.BLACK;
		clickState = 0;
		rotation = 0;
	}
	public Entity(int x,int y, String n, String filename){
		xPos = x;
		yPos = y;
		name = n;
		col = Color.BLACK;
		clickState = 0;
		rotation = 0;
		BufferedImage funt =null;
		try {
			funt = ImageIO.read(new File(filename));
		} catch (IOException e) {
		}
		int w = funt.getWidth();
		int h = funt.getHeight();
		
		Point i1 = new Point(0, 0);
		Point i2 = new Point(w, 0);
		Point i3 = new Point(w, h);
		Point i4 = new Point(0, h);
		Point[] boint = { i1, i2, i3, i4 };
		vertexes = boint;
		originalVertexes = boint;
		
		texture = funt;
		collideable = false;
		centre = new Point(0,0);
		halfX = Math.abs(vertexes[0].x - vertexes[1].x)/2;
		halfY = Math.abs(vertexes[0].y - vertexes[2].y)/2;
	}
	
	public Point[] getOriginalPoints(){
		return originalVertexes;
	}
	
	public Point[] getTranslatedVertexes(){
		Point[] tempVert = new Point[this.vertexes.length];
		for(int i = 0; i < this.vertexes.length;i++){
			tempVert[i]  = (Point)this.vertexes[i].clone();
		}
		for(Point i: tempVert){
			i.x += this.xPos;
			i.y += this.yPos;
		}
		return tempVert;
	}
	public void update(KeybMouse keybstar){
		this.clickState = calculateLeftClickState(new Point(keybstar.xCoord, keybstar.yCoord),keybstar.lmb);
		xPos += xVel;
		yPos += yVel;
	}
	
	public int calculateLeftClickState(Point mouse,boolean click){
		if(click){
			System.out.println("CLICK");
		}
		int lastState = this.clickState;
		int c = 0;
		
		Polygon thispoly = new Polygon();
		for(int i = 0; i < vertexes.length;i++){
			thispoly.addPoint(this.vertexes[i].x + (int)this.xPos,this.vertexes[i].y +(int)this.yPos);
		}
		if(thispoly.contains(mouse)){
			c = 1;
			if(click){
				c = 2;
			}
		 }
		
		
		//releasing after clicking on object
		if(lastState == 2 && c == 1)return 3;
		//Hovering
		if(c == 1)return c;
		//Clicking Down
		if(c == 2)return c;
		//releasing away from the object
		return 0;
	}
	
	public void updateTexture(String filename){
		BufferedImage img = null;
		try {
		    img = ImageIO.read(new File(filename));
		    this.texture = img;
		} catch (IOException e) {
		}
	}

	public ArrayList<String> getClickOptions(){
		ArrayList<String> al = new ArrayList<String>();
		al.add("inspect");
		return al;
	}
	public Entity collisionCheck(Entity e){
		Polygon thispoly = new Polygon();
		for(int i = 0; i < vertexes.length;i++){
			thispoly.addPoint(this.vertexes[i].x,this.vertexes[i].y);
		}
		for(Point p : e.vertexes){
			if(thispoly.contains(p)){
				//System.out.println("COLLISION" + p);
			}
		}
		
		return e;
	}
	
	public Vertex[] getNorms(Polygon p){
		Vertex[] normals = new Vertex[p.npoints];
		for(int i = 1; i < p.npoints - 1; i++){
			
		}
		
		return normals;
	}
	
	public void setVel(double x, double y){
		xVel = x;
		yVel = y;
	}
	public void rotatePointMatrix(Point[] original, double angle, Point[] storeTo,Point centre){
		 AffineTransform.getRotateInstance
	        (Math.toRadians(angle), centre.x, centre.y)
	                .transform(original,0,storeTo,0,original.length);
	}
	
	public Graphics draw(Graphics g, GameLoopTest.GamePanel gp, Camera c){
		centre = findCentre(vertexes);
		//rotation ++;
		if(texture!= null){
			if(rotation >= 360){
				rotation = rotation - 360;
			}
			AffineTransform identity = new AffineTransform();

			Graphics2D g2d = (Graphics2D)g;
			AffineTransform trans = new AffineTransform();
			trans.setTransform(identity);
			
			trans.translate(xPos, yPos);
			trans.translate(-c.x, -c.y);
			trans.rotate( Math.toRadians(rotation) );
			//trans.translate(-halfY,-halfX);
			
			//ADD SCALING?
			if(scale != 1){
				Image im = texture.getScaledInstance((int)(scale*(halfX *2)),(int)( scale*(halfY *2)), Image.SCALE_FAST);
				System.out.println((int)(scale*(halfX *2)));
				System.out.println((int)( scale*(halfY *2)));
				BufferedImage newImage = new BufferedImage(
				im.getWidth(null), im.getHeight(null),
				        BufferedImage.TYPE_INT_ARGB);
				Graphics2D b = newImage.createGraphics();
				b.drawImage(im, 0, 0, null);
				b.dispose();
				texture = newImage;
			}
			
			
			g2d.drawImage((Image)texture, trans, gp);
			
			return g;
		}
		Polygon poly = new Polygon();
		Point[] rotated = new Point[vertexes.length];
		for(int i = 0; i < vertexes.length;i++){
			rotated[i] = new Point(0,0);
		}
		rotatePointMatrix(vertexes, rotation,rotated,centre);
		for(int i = 0; i < vertexes.length;i++){
			poly.addPoint((int)(rotated[i].x), (int)(rotated[i].y));
		}
		poly.translate((int)(xPos - c.x),(int)(yPos -c.y));
		g.setColor(this.col);
		g.fillPolygon(poly);
		return g;
	}
	
	public Point findCentre(Point[] ver){
		//for(int i = 0; i < ver.length;i++){
		//	ver[i].x = ver[i].x + (int)xPos;
		//	ver[i].y = ver[i].y + (int)yPos;
		//}
		double sumX = 0;
		double sumY = 0;
		for(int i = 0; i < ver.length;i++){
			sumX += ver[i].x;
			sumY += ver[i].y;
		}
		Point res = new Point((int)(sumX/ver.length) + (int)xPos,(int)(sumY/ver.length) + (int)yPos);
		
		res.x = (int)(this.halfX + this.xPos);
		res.y = (int)(this.halfY + this.yPos);
		return res;
	}
	
	public Command checkClick(Point mousePos){
		Command c = new Command("null");
		if(Math.sqrt((mousePos.x - this.centre.x)*(mousePos.x - this.centre.x) +(mousePos.y - this.centre.y)*(mousePos.y - this.centre.y)) < 100){
			c = doClick();
		}
		return c; 
	}
	
	public Command doClick(){
		return new Command("null");
	}
	
	public void scaleToScreen(int width, int height){
		double w = this.halfX * 2;
		scale = width/w;
	}
	
	public GameLoopTest interact(GameLoopTest g){
		//CREATE AN OBJECT TO DISPLAY ON SCREEN, MAKE G DRAW IT
		return g;
	}
	
	public boolean isClick(Point mouse){
		boolean c = false;
		
		Polygon thispoly = new Polygon();
		for(int i = 0; i < vertexes.length;i++){
			thispoly.addPoint(this.vertexes[i].x + (int)xPos,this.vertexes[i].y + (int)yPos);
		}
		if(thispoly.contains(mouse)){
			c = true;
		}
		
		return c;
	}
	
}
