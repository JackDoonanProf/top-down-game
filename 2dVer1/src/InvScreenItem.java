import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

public class InvScreenItem extends Entity {

	int screenOrder;
	String text;
	//0 = away, 1 = hover, 2 = clickDown, 3 = released
	int clickState;
	
	String standardFilename;
	String hoverFilename;
	String clickFilename;
	
	public InvScreenItem(int x, int y, String n) {
		super(x, y, n,"defaultIcon.png");
		
		standardFilename = n + "StandardIcon.png";
		hoverFilename = n + "HoverIcon.png";
		clickFilename = n+ "ClickIcon.png";
		this.screenOrder = 1;
		this.clickState = 0;
	}
	public void setText(String t){
		this.text = t;
	}
	public void update(KeybMouse kb){
		super.update(kb);
		this.clickState = this.calculateLeftClickState(new Point(kb.xCoord, kb.yCoord),kb.lmb);
		
		if(clickState == 0 || clickState == 3){
			this.updateTexture(standardFilename);
		}
		if(clickState == 1){
			this.updateTexture(hoverFilename);
		}
		if(clickState == 2){
			this.updateTexture(clickFilename);
		}
	}
	public Graphics draw(Graphics g, GameLoopTest.GamePanel gp, Camera c){
		//rotation ++;
		int screenWidth = c.SCREEN_WIDTH;
		int screenHeight = c.SCREEN_HEIGHT;
		Polygon poly = new Polygon();

		int xLeft = (int)(.1*screenWidth);
		this.xPos = xLeft;
		int xRight = (int)(.25*screenWidth);
		int yTop = (int) (.2*screenHeight + (60 * screenOrder));
		this.yPos = yTop;
		int yBot = yTop + 60;
		
		poly.addPoint(xLeft,yTop);
		vertexes[0] = new Point(xLeft,yTop);
		poly.addPoint(xRight,yTop);
		vertexes[1] = new Point(xRight,yTop);
		poly.addPoint(xRight,yBot);
		vertexes[2] = new Point(xRight,yBot);
		poly.addPoint(xLeft,yBot);
		vertexes[3] = new Point(xLeft,yBot);
		
		g.setColor(Color.GRAY);
		g.fillPolygon(poly);
		
		Image im = texture.getScaledInstance((int)(.20*screenWidth),60, Image.SCALE_FAST);
		BufferedImage newImage = new BufferedImage(
		im.getWidth(null), im.getHeight(null),
		        BufferedImage.TYPE_INT_ARGB);
		Graphics2D b = newImage.createGraphics();
		b.drawImage(im, 0, 0, null);
		b.dispose();
		texture = newImage;
		
		g.drawImage((Image)texture,xLeft,yTop,gp);
		
		g.setColor(Color.BLACK);
		if(screenOrder == 0){
			g.setColor(Color.YELLOW);
		}
		g.drawString(this.text, xLeft + 10, yTop + 10);
		return g;
	}
	public ArrayList<String> getClickOptions(){
		ArrayList<String>  al = new ArrayList<String> ();
		al.add("inspect");
		al.add("equip");
		al.add("drop");
		return al;
	}
	public int calculateLeftClickState(Point mouse,boolean click){
		if(click){
			System.out.println("CLICK");
		}
		int lastState = this.clickState;
		int c = 0;
		
		Polygon thispoly = new Polygon();
		for(int i = 0; i < vertexes.length;i++){
			thispoly.addPoint(this.vertexes[i].x ,this.vertexes[i].y );
		}
		if(thispoly.contains(mouse)){
			c = 1;
			if(click){
				c = 2;
			}
		 }
		
		
		//releasing after clicking on object
		if(lastState == 2 && c == 1)return 3;
		//Hovering
		if(c == 1)return c;
		//Clicking Down
		if(c == 2)return c;
		//releasing away from the object
		return 0;
	}
}
